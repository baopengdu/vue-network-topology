# Library

## 基础用法

这是一个图形库的组件案例

> 通过 `selects` 配置下拉框选项

```html
<template>
  <library :selects="selects" @library="handleLibrary">Library</library>
</template>

<script>
  export default {
    data() {
      return {
        selects: [
          {
            label: "风格1",
            value: "style1",
          },
          {
            label: "风格2",
            value: "style2",
          },
        ],
      };
    },
    methods: {
      handleLibrary(value) {
        console.log('value', value)
      }
    }
  };
</script>
```
:::

## 属性参数

|参数|说明|类型|可选值|默认值|
|:-:|:-:|:-:|:-:|:-:|
|selects|样式选择框|`Array`|--|`() => [{label:'',value:''}]`|

## 事件方法

|事件|说明|回调参数|
|:-:|:-:|:-:|
|library|样式选择框变化后的函数|`(value) => void`|
