# Tool

## 基础用法

这是一个工具区的组件案例

::: demo 工具区通用配置，无入参

```html
<template>
  <tool>{{content}}</tool>
</template>

<script>
  export default {
    data() {
      return {
        content: '工具区组件'
      }
    },
  };
</script>
```

:::

## 属性参数

|参数|说明|类型|可选值|默认值|
|:-:|:-:|:-:|:-:|:-:|