# GroupAttr

## 基础用法

这是一个组的组件案例

> 通过 `base`、 `group` 属性设置不同类型的配置

```html
<template>
  <group-attr/>
</template>

<script>
  export default {
    data() {
      return {  
      }
    },
  };
</script>
```

:::

## 属性参数

|参数|说明|类型|可选值|默认值|
|:-:|:-:|:-:|:-:|:-:|
|base|基础属性|``Array``|--|`() => [{key: 'font',value: 13},{key: 'color',value: '#000000'},{key: 'position',value: 'bottom'}]`|
|group|组属性|``Array``|--|`() => [{key: 'expand',value: '1'},{key: 'shape',value: 'rectangle'},{key: 'fill',value: '#D6E0FF'}]`|
