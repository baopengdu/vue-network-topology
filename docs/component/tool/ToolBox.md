# ToolBox

## 基础用法

这是一个工具区区块的组件案例

> 通过 `icon` 属性设置工具区块的图标

```html
<template>
  <tool>
    <tool-box :icon="icon">
        <span>{{title}}</span>
    </tool-box>
  </tool>
</template>

<script>
  export default {
    data() {
      return {
        icon: 'http://10.186.2.55:8148/tgcos/582717604620:vwaver/icon-selection.svg',
        title: '选择'
      }
    },
  };
</script>
```

:::

## 属性参数

|参数|说明|类型|可选值|默认值|
|:-:|:-:|:-:|:-:|:-:|
|icon|图标连接|``String``|--|--|