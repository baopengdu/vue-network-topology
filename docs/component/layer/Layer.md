# Layer

## 基础用法

这是一个拓扑树的组件案例

> 通过 `treeData` 进行拓扑树层级的传参

```html
<template>
  <layer :treeData="layerData" @choose="handleChoose" />
</template>

<script>
  export default {
    data() {
      return {
        layerData: [
          {
            title: "0-0",
            key: "0-0",
            children: [
              {
                title: "0-0-0",
                key: "0-0-0",
                children: [
                  { title: "0-0-0-0", key: "0-0-0-0" },
                  { title: "0-0-0-1", key: "0-0-0-1" },
                  { title: "0-0-0-2", key: "0-0-0-2" },
                ],
              },
              {
                title: "0-0-1",
                key: "0-0-1",
                children: [
                  { title: "0-0-1-0", key: "0-0-1-0" },
                  { title: "0-0-1-1", key: "0-0-1-1" },
                  { title: "0-0-1-2", key: "0-0-1-2" },
                ],
              },
              {
                title: "0-0-2",
                key: "0-0-2",
              },
            ],
          },
          {
            title: "0-1",
            key: "0-1",
            children: [
              { title: "0-1-0-0", key: "0-1-0-0" },
              { title: "0-1-0-1", key: "0-1-0-1" },
              { title: "0-1-0-2", key: "0-1-0-2" },
            ],
          },
          {
            title: "0-2",
            key: "0-2",
          },
        ],
      };
    },
    methods: {
      handleChoose(key) {
        console.log("key", key);
      },
    },
  };
</script>
```

:::

## 属性参数

|参数|说明|类型|可选值|默认值|
|:-:|:-:|:-:|:-:|:-:|
|treeData|拓扑树数据|`Array`|--|`() => [{ key:'', title:'', children:[] }]`|

## 事件方法

|事件|说明|回调参数|
|:-:|:-:|:-:|
|choose|点击节点操作的函数|`(key) => void`|
