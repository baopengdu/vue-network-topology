'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

require('core-js/modules/es.function.name.js');
require('core-js/modules/es.object.to-string.js');
require('core-js/modules/web.dom-collections.for-each.js');
var antDesignVue = require('ant-design-vue');
require('core-js/modules/es.array.join.js');
require('core-js/modules/es.array.filter.js');
require('core-js/modules/es.array.splice.js');
require('core-js/modules/es.array.includes.js');
require('core-js/modules/es.string.includes.js');
require('core-js/modules/es.array.push.js');
require('core-js/modules/es.array.map.js');
require('core-js/modules/es.array.concat.js');
require('core-js/modules/es.array.at.js');
require('core-js/modules/es.string.at-alternative.js');
require('core-js/modules/es.json.stringify.js');

//
//
//
//
//
//

var script$j = {
  name: 'Attr'
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}

const isOldIE = typeof navigator !== 'undefined' &&
    /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
    return (id, style) => addStyle(id, style);
}
let HEAD;
const styles = {};
function addStyle(id, css) {
    const group = isOldIE ? css.media || 'default' : id;
    const style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
    if (!style.ids.has(id)) {
        style.ids.add(id);
        let code = css.source;
        if (css.map) {
            // https://developer.chrome.com/devtools/docs/javascript-debugging
            // this makes source maps inside style tags work properly in Chrome
            code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
            // http://stackoverflow.com/a/26603875
            code +=
                '\n/*# sourceMappingURL=data:application/json;base64,' +
                    btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                    ' */';
        }
        if (!style.element) {
            style.element = document.createElement('style');
            style.element.type = 'text/css';
            if (css.media)
                style.element.setAttribute('media', css.media);
            if (HEAD === undefined) {
                HEAD = document.head || document.getElementsByTagName('head')[0];
            }
            HEAD.appendChild(style.element);
        }
        if ('styleSheet' in style.element) {
            style.styles.push(code);
            style.element.styleSheet.cssText = style.styles
                .filter(Boolean)
                .join('\n');
        }
        else {
            const index = style.ids.size - 1;
            const textNode = document.createTextNode(code);
            const nodes = style.element.childNodes;
            if (nodes[index])
                style.element.removeChild(nodes[index]);
            if (nodes.length)
                style.element.insertBefore(textNode, nodes[index]);
            else
                style.element.appendChild(textNode);
        }
    }
}

/* script */
const __vue_script__$j = script$j;

/* template */
var __vue_render__$i = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("div", { staticClass: "attr" }, [_vm._t("default")], 2)
};
var __vue_staticRenderFns__$i = [];
__vue_render__$i._withStripped = true;

  /* style */
  const __vue_inject_styles__$j = function (inject) {
    if (!inject) return
    inject("data-v-552d9c9e_0", { source: ".attr[data-v-552d9c9e] {\n  padding: 0 20px;\n}\n", map: {"version":3,"sources":["Attr.vue"],"names":[],"mappings":"AAAA;EACE,eAAe;AACjB","file":"Attr.vue","sourcesContent":[".attr {\n  padding: 0 20px;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$j = "data-v-552d9c9e";
  /* module identifier */
  const __vue_module_identifier__$j = undefined;
  /* functional template */
  const __vue_is_functional_template__$j = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$j = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$i, staticRenderFns: __vue_staticRenderFns__$i },
    __vue_inject_styles__$j,
    __vue_script__$j,
    __vue_scope_id__$j,
    __vue_is_functional_template__$j,
    __vue_module_identifier__$j,
    false,
    createInjector,
    undefined,
    undefined
  );

function _typeof(obj) {
  "@babel/helpers - typeof";

  return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, _typeof(obj);
}

function _toPrimitive(input, hint) {
  if (_typeof(input) !== "object" || input === null) return input;
  var prim = input[Symbol.toPrimitive];
  if (prim !== undefined) {
    var res = prim.call(input, hint || "default");
    if (_typeof(res) !== "object") return res;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }
  return (hint === "string" ? String : Number)(input);
}

function _toPropertyKey(arg) {
  var key = _toPrimitive(arg, "string");
  return _typeof(key) === "symbol" ? key : String(key);
}

function _defineProperty(obj, key, value) {
  key = _toPropertyKey(key);
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

var positionSelects = [{
  value: 'bottom.bottom',
  label: '底部'
}, {
  value: 'top',
  label: '顶部'
}, {
  value: 'center',
  label: '居中'
}, {
  value: 'left',
  label: '左侧'
}, {
  value: 'right',
  label: '右侧'
}];
var script$i = {
  name: 'BaseForm',
  props: {
    label: {
      type: String,
      default: ''
    },
    base: {
      type: Array,
      default: function _default() {
        return [
          // {
          //   key: 'font',
          //   value: 13,
          // },
          // {
          //   key: 'color',
          //   value: '#000000',
          // },
          // {
          //   key: 'position',
          //   value: positionSelects[0].value,
          // },
        ];
      }
    }
  },
  components: {
    Form: antDesignVue.Form,
    FormItem: antDesignVue.Form.Item,
    Input: antDesignVue.Input,
    InputNumber: antDesignVue.InputNumber,
    Select: antDesignVue.Select,
    Option: antDesignVue.Select.Option
  },
  data: function data() {
    return {
      form: this.$form.createForm(this, {
        name: 'common'
      }),
      selects: positionSelects
    };
  },
  methods: {},
  watch: {
    base: {
      deep: true,
      immediate: true,
      handler: function handler(newValue) {
        var _this = this;
        if (!newValue) return;
        this.$nextTick(function () {
          newValue.forEach(function (_ref) {
            var key = _ref.key,
              value = _ref.value;
            _this.form.setFieldsValue(_defineProperty({}, key, value));
          });
        });
      }
    }
  }
};

/* script */
const __vue_script__$i = script$i;

/* template */
var __vue_render__$h = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "Form",
    {
      attrs: {
        form: _vm.form,
        "label-col": { span: 4 },
        "wrapper-col": { span: 20 },
        labelAlign: "left",
        autocomplete: "off",
      },
    },
    [
      _c(
        "FormItem",
        { attrs: { label: "名称" } },
        [
          _c("Input", {
            directives: [
              {
                name: "decorator",
                rawName: "v-decorator",
                value: ["label"],
                expression: "['label']",
              },
            ],
            attrs: { placeholder: _vm.label },
            nativeOn: {
              change: function ($event) {
                return function (e) {
                  return _vm.$emit("label", _vm.form.getFieldValue("label"))
                }.apply(null, arguments)
              },
            },
          }),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticStyle: { display: "flex" } },
        [
          _c(
            "FormItem",
            {
              staticStyle: { flex: "1" },
              attrs: {
                "label-col": { span: 8 },
                "wrapper-col": { span: 8 },
                label: "字号",
              },
            },
            [
              _c("InputNumber", {
                directives: [
                  {
                    name: "decorator",
                    rawName: "v-decorator",
                    value: ["font"],
                    expression: "['font']",
                  },
                ],
                on: {
                  change: function (value) {
                    return _vm.$emit("font", value)
                  },
                },
              }),
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "FormItem",
            {
              staticStyle: {
                flex: "1",
                display: "flex",
                "justify-content": "right",
              },
              attrs: {
                "label-col": { span: 8 },
                "wrapper-col": { span: 12 },
                label: "颜色",
              },
            },
            [
              _c("Input", {
                directives: [
                  {
                    name: "decorator",
                    rawName: "v-decorator",
                    value: ["color"],
                    expression: "['color']",
                  },
                ],
                attrs: { type: "color" },
                nativeOn: {
                  change: function ($event) {
                    return function (e) {
                      return _vm.$emit("color", e.target._value)
                    }.apply(null, arguments)
                  },
                },
              }),
            ],
            1
          ),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "FormItem",
        { attrs: { label: "位置" } },
        [
          _c(
            "Select",
            {
              directives: [
                {
                  name: "decorator",
                  rawName: "v-decorator",
                  value: ["position"],
                  expression: "['position']",
                },
              ],
              on: {
                change: function (value) {
                  return _vm.$emit("position", value)
                },
              },
            },
            _vm._l(_vm.selects, function (item) {
              return _c(
                "Option",
                { key: item.value, attrs: { value: item.value } },
                [_vm._v("\n        " + _vm._s(item.label) + "\n      ")]
              )
            }),
            1
          ),
        ],
        1
      ),
    ],
    1
  )
};
var __vue_staticRenderFns__$h = [];
__vue_render__$h._withStripped = true;

  /* style */
  const __vue_inject_styles__$i = undefined;
  /* scoped */
  const __vue_scope_id__$i = "data-v-98fa6cbc";
  /* module identifier */
  const __vue_module_identifier__$i = undefined;
  /* functional template */
  const __vue_is_functional_template__$i = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$i = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$h, staticRenderFns: __vue_staticRenderFns__$h },
    __vue_inject_styles__$i,
    __vue_script__$i,
    __vue_scope_id__$i,
    __vue_is_functional_template__$i,
    __vue_module_identifier__$i,
    false,
    undefined,
    undefined,
    undefined
  );

//
var script$h = {
  name: 'NodeAttr',
  props: {
    node: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  components: {
    BaseForm: __vue_component__$i
  }
};

/* script */
const __vue_script__$h = script$h;

/* template */
var __vue_render__$g = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "node-attr" },
    [
      _c("BaseForm", {
        attrs: { label: "请输入网元名称", base: _vm.node },
        on: {
          label: function (value) {
            return _vm.$emit("nodeLabel", value)
          },
          font: function (value) {
            return _vm.$emit("nodeFont", value)
          },
          color: function (value) {
            return _vm.$emit("nodeColor", value)
          },
          position: function (value) {
            return _vm.$emit("nodePosition", value)
          },
        },
      }),
    ],
    1
  )
};
var __vue_staticRenderFns__$g = [];
__vue_render__$g._withStripped = true;

  /* style */
  const __vue_inject_styles__$h = undefined;
  /* scoped */
  const __vue_scope_id__$h = "data-v-d1168c18";
  /* module identifier */
  const __vue_module_identifier__$h = undefined;
  /* functional template */
  const __vue_is_functional_template__$h = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$h = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$g, staticRenderFns: __vue_staticRenderFns__$g },
    __vue_inject_styles__$h,
    __vue_script__$h,
    __vue_scope_id__$h,
    __vue_is_functional_template__$h,
    __vue_module_identifier__$h,
    false,
    undefined,
    undefined,
    undefined
  );

//
var script$g = {
  name: 'SubNetworkAttr',
  props: {
    subNetwork: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  components: {
    BaseForm: __vue_component__$i
  }
};

/* script */
const __vue_script__$g = script$g;

/* template */
var __vue_render__$f = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "subnetwork-attr" },
    [
      _c("BaseForm", {
        attrs: { label: "请输入子网名称", base: _vm.subNetwork },
        on: {
          label: function (value) {
            return _vm.$emit("subNetworkLabel", value)
          },
          font: function (value) {
            return _vm.$emit("subNetworkFont", value)
          },
          color: function (value) {
            return _vm.$emit("subNetworkColor", value)
          },
          position: function (value) {
            return _vm.$emit("subNetworkPosition", value)
          },
        },
      }),
    ],
    1
  )
};
var __vue_staticRenderFns__$f = [];
__vue_render__$f._withStripped = true;

  /* style */
  const __vue_inject_styles__$g = undefined;
  /* scoped */
  const __vue_scope_id__$g = "data-v-50da00cb";
  /* module identifier */
  const __vue_module_identifier__$g = undefined;
  /* functional template */
  const __vue_is_functional_template__$g = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$g = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$f, staticRenderFns: __vue_staticRenderFns__$f },
    __vue_inject_styles__$g,
    __vue_script__$g,
    __vue_scope_id__$g,
    __vue_is_functional_template__$g,
    __vue_module_identifier__$g,
    false,
    undefined,
    undefined,
    undefined
  );

//
//
//
//
//
//
//
//
//

var script$f = {
  name: 'CommonSpace',
  props: {
    name: {
      type: String,
      default: ''
    }
  }
};

/* script */
const __vue_script__$f = script$f;

/* template */
var __vue_render__$e = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "common-space" },
    [
      _c("div", { staticClass: "common-space-name" }, [
        _c("span", [_vm._v(_vm._s(_vm.name))]),
      ]),
      _vm._v(" "),
      _vm._t("default"),
    ],
    2
  )
};
var __vue_staticRenderFns__$e = [];
__vue_render__$e._withStripped = true;

  /* style */
  const __vue_inject_styles__$f = function (inject) {
    if (!inject) return
    inject("data-v-8faa0112_0", { source: ".common-space[data-v-8faa0112] {\n  border-top: 1px dashed #979797;\n}\n.common-space .common-space-name[data-v-8faa0112] {\n  margin: 20px 0;\n  color: #333;\n  font-size: 14px;\n}\n", map: {"version":3,"sources":["CommonSpace.vue"],"names":[],"mappings":"AAAA;EACE,8BAA8B;AAChC;AACA;EACE,cAAc;EACd,WAAW;EACX,eAAe;AACjB","file":"CommonSpace.vue","sourcesContent":[".common-space {\n  border-top: 1px dashed #979797;\n}\n.common-space .common-space-name {\n  margin: 20px 0;\n  color: #333;\n  font-size: 14px;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$f = "data-v-8faa0112";
  /* module identifier */
  const __vue_module_identifier__$f = undefined;
  /* functional template */
  const __vue_is_functional_template__$f = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$f = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$e, staticRenderFns: __vue_staticRenderFns__$e },
    __vue_inject_styles__$f,
    __vue_script__$f,
    __vue_scope_id__$f,
    __vue_is_functional_template__$f,
    __vue_module_identifier__$f,
    false,
    createInjector,
    undefined,
    undefined
  );

var patternSelects = [{
    value: 'solid',
    label: 'solid'
  }, {
    value: 'dashed_1_1',
    label: 'dashed_1_1'
  }, {
    value: 'dashed_2_2',
    label: 'dashed_2_2'
  }, {
    value: 'dashed_4_4',
    label: 'dashed_4_4'
  }, {
    value: 'dashed_5_10',
    label: 'dashed_5_10'
  }],
  widthSelects = [{
    value: '0.25',
    label: '0.25 pt'
  }, {
    value: '0.75',
    label: '0.75 pt'
  }, {
    value: '1',
    label: '1 pt'
  }, {
    value: '2',
    label: '2 pt'
  }, {
    value: '3',
    label: '3 pt'
  }, {
    value: '4',
    label: '4 pt'
  }, {
    value: '5',
    label: '5 pt'
  }, {
    value: '6',
    label: '6 pt'
  }],
  typeSelects = [{
    value: 'arc',
    label: '直线'
  }, {
    value: 'orthogonal.V.H',
    label: '折线'
  }];
var script$e = {
  name: 'LinkAttr',
  components: {
    BaseForm: __vue_component__$i,
    CommonSpace: __vue_component__$f,
    // ant design vue
    Form: antDesignVue.Form,
    FormItem: antDesignVue.Form.Item,
    Input: antDesignVue.Input,
    Select: antDesignVue.Select,
    Option: antDesignVue.Select.Option,
    Dropdown: antDesignVue.Dropdown,
    Menu: antDesignVue.Menu,
    MenuItem: antDesignVue.Menu.Item
  },
  props: {
    base: {
      type: Array,
      default: function _default() {
        return [{
          key: 'font',
          value: 13
        }, {
          key: 'color',
          value: '#000000'
        }, {
          key: 'position',
          value: 'bottom'
        }];
      }
    },
    link: {
      type: Array,
      default: function _default() {
        return [{
          key: 'pattern',
          value: 'solid'
        }, {
          key: 'tint',
          value: '#658DC1'
        }, {
          key: 'width',
          value: '3'
        }, {
          key: 'type',
          value: 'arc'
        }];
      }
    },
    typeMap: {
      type: Object,
      default: function _default() {}
    }
  },
  data: function data() {
    return {
      form: this.$form.createForm(this, {
        name: 'link'
      }),
      selects: {
        pattern: patternSelects,
        width: widthSelects,
        type: typeSelects
      },
      pattern: patternSelects[0].value,
      width: widthSelects[0].value,
      type: typeSelects[0].value
    };
  },
  watch: {
    link: {
      deep: true,
      immediate: true,
      handler: function handler(newValue) {
        var _this = this;
        if (newValue) {
          this.$nextTick(function () {
            newValue.forEach(function (_ref) {
              var key = _ref.key,
                value = _ref.value;
              if (key == 'tint') {
                _this.form.setFieldsValue(_defineProperty({}, "".concat(key), value));
              } else {
                _this["".concat(key)] = value;
              }
            });
          });
        }
      }
    }
  },
  methods: {
    handlePattern: function handlePattern(_ref2) {
      var key = _ref2.key;
      this.pattern = key;
      this.$emit("linkPattern", key);
    },
    handleWidth: function handleWidth(_ref3) {
      var key = _ref3.key;
      this.width = key;
      this.$emit("linkWidth", key);
    },
    handleType: function handleType(_ref4) {
      var key = _ref4.key;
      this.type = key;
      this.$emit("linkType", key);
    }
  },
  computed: {
    computeWidth: function computeWidth() {
      return function (width) {
        return width.split('.').join('_');
      };
    }
  },
  filters: {
    typeFilter: function typeFilter(v) {
      return typeSelects.filter(function (f) {
        return f.value == v;
      })[0].label;
    }
  }
};

/* script */
const __vue_script__$e = script$e;

/* template */
var __vue_render__$d = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "link-attr" },
    [
      _c("BaseForm", {
        attrs: { label: "请输入连接线名称", base: _vm.base },
        on: {
          label: function (value) {
            return _vm.$emit("linkLabel", value)
          },
          font: function (value) {
            return _vm.$emit("linkFont", value)
          },
          color: function (value) {
            return _vm.$emit("linkColor", value)
          },
          position: function (value) {
            return _vm.$emit("linkPosition", value)
          },
        },
      }),
      _vm._v(" "),
      _c(
        "CommonSpace",
        { attrs: { name: "线段" } },
        [
          _c("Form", { attrs: { form: _vm.form, autocomplete: "off" } }, [
            _c(
              "div",
              { staticStyle: { display: "flex" } },
              [
                _c(
                  "FormItem",
                  {
                    staticStyle: {
                      flex: "1",
                      display: "flex",
                      "align-items": "center",
                    },
                    attrs: {
                      "label-col": { span: 8 },
                      "wrapper-col": { span: 16 },
                      label: "虚线",
                    },
                  },
                  [
                    _c(
                      "Dropdown",
                      [
                        _c(
                          "div",
                          { staticClass: "link-attr-pattern-wrapper" },
                          [
                            _c("div", {
                              class:
                                "link-attr-pattern link-attr-pattern-" +
                                _vm.pattern +
                                " link-attr-pattern-tricker",
                            }),
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "Menu",
                          {
                            attrs: { slot: "overlay" },
                            on: { click: _vm.handlePattern },
                            slot: "overlay",
                          },
                          _vm._l(_vm.selects.pattern, function (item) {
                            return _c(
                              "MenuItem",
                              { key: item.value, attrs: { value: item.value } },
                              [
                                _c("div", {
                                  class:
                                    "link-attr-pattern link-attr-pattern-" +
                                    item.label,
                                }),
                              ]
                            )
                          }),
                          1
                        ),
                      ],
                      1
                    ),
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "FormItem",
                  {
                    staticStyle: {
                      flex: "1",
                      display: "flex",
                      "justify-content": "right",
                      "align-items": "center",
                    },
                    attrs: {
                      "label-col": { span: 8 },
                      "wrapper-col": { span: 12 },
                      label: "颜色",
                    },
                  },
                  [
                    _c("Input", {
                      directives: [
                        {
                          name: "decorator",
                          rawName: "v-decorator",
                          value: ["tint"],
                          expression: "['tint']",
                        },
                      ],
                      attrs: { type: "color" },
                      nativeOn: {
                        change: function ($event) {
                          return function (e) {
                            return _vm.$emit("linkTint", e.target._value)
                          }.apply(null, arguments)
                        },
                      },
                    }),
                  ],
                  1
                ),
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticStyle: { display: "flex" } },
              [
                _c(
                  "FormItem",
                  {
                    staticStyle: {
                      flex: "1",
                      display: "flex",
                      "align-items": "center",
                    },
                    attrs: {
                      "label-col": { span: 8 },
                      "wrapper-col": { span: 16 },
                      label: "粗细",
                    },
                  },
                  [
                    _c(
                      "Dropdown",
                      [
                        _c("div", { staticClass: "link-attr-width-wrapper" }, [
                          _c("div", {
                            class:
                              "link-attr-width-" +
                              _vm.computeWidth(_vm.width) +
                              " link-attr-width link-attr-width-tricker",
                          }),
                        ]),
                        _vm._v(" "),
                        _c(
                          "Menu",
                          {
                            attrs: { slot: "overlay" },
                            on: { click: _vm.handleWidth },
                            slot: "overlay",
                          },
                          _vm._l(_vm.selects.width, function (item) {
                            return _c(
                              "MenuItem",
                              { key: item.value, attrs: { value: item.value } },
                              [
                                _c(
                                  "div",
                                  { staticClass: "link-attr-width-box" },
                                  [
                                    _c("div", {
                                      class:
                                        "link-attr-width-" +
                                        _vm.computeWidth(item.value) +
                                        " link-attr-width",
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "span",
                                      { staticClass: "link-attr-width-label" },
                                      [
                                        _vm._v(
                                          "\n                    " +
                                            _vm._s(item.label) +
                                            "\n                  "
                                        ),
                                      ]
                                    ),
                                  ]
                                ),
                              ]
                            )
                          }),
                          1
                        ),
                      ],
                      1
                    ),
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "FormItem",
                  {
                    staticStyle: {
                      flex: "1",
                      display: "flex",
                      "justify-content": "right",
                      "align-items": "center",
                    },
                    attrs: {
                      "label-col": { span: 8 },
                      "wrapper-col": { span: 12 },
                      label: "样式",
                    },
                  },
                  [
                    _c(
                      "Dropdown",
                      [
                        _c("div", { staticClass: "link-attr-type-wrapper" }, [
                          _c(
                            "span",
                            { staticClass: "link-attr-type-tricker" },
                            [
                              _vm._v(
                                "\n                " +
                                  _vm._s(_vm._f("typeFilter")(_vm.type)) +
                                  "\n              "
                              ),
                            ]
                          ),
                        ]),
                        _vm._v(" "),
                        _c(
                          "Menu",
                          {
                            attrs: { slot: "overlay" },
                            on: { click: _vm.handleType },
                            slot: "overlay",
                          },
                          _vm._l(_vm.selects.type, function (item) {
                            return _c(
                              "MenuItem",
                              { key: item.value, attrs: { value: item.value } },
                              [
                                _c(
                                  "div",
                                  { staticClass: "link-attr-type-box" },
                                  [
                                    _c("img", {
                                      staticClass: "link-attr-type",
                                      attrs: {
                                        src: _vm.typeMap[item.value],
                                        alt: "icon-" + item.value,
                                      },
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "span",
                                      { staticClass: "link-attr-type-label" },
                                      [
                                        _vm._v(
                                          "\n                    " +
                                            _vm._s(item.label) +
                                            "\n                  "
                                        ),
                                      ]
                                    ),
                                  ]
                                ),
                              ]
                            )
                          }),
                          1
                        ),
                      ],
                      1
                    ),
                  ],
                  1
                ),
              ],
              1
            ),
          ]),
        ],
        1
      ),
    ],
    1
  )
};
var __vue_staticRenderFns__$d = [];
__vue_render__$d._withStripped = true;

  /* style */
  const __vue_inject_styles__$e = function (inject) {
    if (!inject) return
    inject("data-v-7195079c_0", { source: ".link-attr-pattern-solid[data-v-7195079c] {\n  background: #2e6ee7;\n  width: 8px;\n}\n.link-attr-pattern-dashed_1_1[data-v-7195079c] {\n  background-size: 1px 1px;\n}\n.link-attr-pattern-dashed_2_2[data-v-7195079c] {\n  background-size: 2px 2px;\n}\n.link-attr-pattern-dashed_4_4[data-v-7195079c] {\n  background-size: 4px 4px;\n}\n.link-attr-pattern-dashed_5_10[data-v-7195079c] {\n  background-size: 5px 10px;\n}\n.link-attr-pattern[data-v-7195079c] {\n  width: 100%;\n  height: 1px;\n  background-image: linear-gradient(to right, #2e6ee7 0%, #2e6ee7 50%, transparent 50%);\n  background-repeat: repeat-x;\n}\n.link-attr-width-box[data-v-7195079c] {\n  display: flex;\n  align-items: center;\n  justify-content: right;\n}\n.link-attr-width[data-v-7195079c] {\n  background-color: #2e6ee7;\n  width: 20px;\n}\n.link-attr-width-label[data-v-7195079c] {\n  color: #666;\n  font-size: 12px;\n  margin-left: 10px;\n  flex: 1;\n}\n.link-attr-width-0_25[data-v-7195079c] {\n  height: 2px;\n}\n.link-attr-width-0_75[data-v-7195079c] {\n  height: 2.5px;\n}\n.link-attr-width-1[data-v-7195079c] {\n  height: 3px;\n}\n.link-attr-width-1_5[data-v-7195079c] {\n  height: 4.5px;\n}\n.link-attr-width-2[data-v-7195079c] {\n  height: 5px;\n}\n.link-attr-width-3[data-v-7195079c] {\n  height: 6px;\n}\n.link-attr-width-4[data-v-7195079c] {\n  height: 7px;\n}\n.link-attr-width-5[data-v-7195079c] {\n  height: 8px;\n}\n.link-attr-width-6[data-v-7195079c] {\n  height: 9px;\n}\n.link-attr-type[data-v-7195079c] {\n  width: 14px;\n  height: 14px;\n}\n.link-attr-type-box[data-v-7195079c] {\n  display: flex;\n  align-items: center;\n}\n.link-attr-type-label[data-v-7195079c] {\n  color: #666;\n  font-size: 12px;\n  margin-left: 10px;\n  flex: 1;\n}\n.link-attr .link-attr-pattern-wrapper[data-v-7195079c] {\n  border: 1px solid #d9d9d9;\n  padding: 15px 10px;\n  border-radius: 4px;\n}\n.link-attr .link-attr-pattern-tricker[data-v-7195079c] {\n  width: 40px;\n  margin: 0 auto;\n  cursor: pointer;\n}\n.link-attr .link-attr-pattern-tricker[data-v-7195079c]::after {\n  display: block;\n  content: '';\n  height: 4px;\n}\n.link-attr .link-attr-pattern-tricker[data-v-7195079c]:hover {\n  cursor: pointer;\n}\n.link-attr .link-attr-width-wrapper[data-v-7195079c] {\n  border: 1px solid #d9d9d9;\n  padding: 0 10px;\n  height: 32px;\n  border-radius: 4px;\n  display: flex;\n  align-items: center;\n}\n.link-attr .link-attr-width-tricker[data-v-7195079c] {\n  width: 40px;\n  margin: 0 auto;\n  cursor: pointer;\n}\n.link-attr .link-attr-width-tricker[data-v-7195079c]::after {\n  display: block;\n  content: '';\n  height: 4px;\n}\n.link-attr .link-attr-width-tricker[data-v-7195079c]:hover {\n  cursor: pointer;\n}\n.link-attr .link-attr-type-wrapper[data-v-7195079c] {\n  border: 1px solid #d9d9d9;\n  padding: 0 10px;\n  height: 32px;\n  border-radius: 4px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-attr .link-attr-type-tricker[data-v-7195079c] {\n  font-size: 14px;\n  color: #666;\n  cursor: pointer;\n}\n.link-attr .link-attr-type-tricker[data-v-7195079c]:hover {\n  cursor: pointer;\n}\n", map: {"version":3,"sources":["LinkAttr.vue"],"names":[],"mappings":"AAAA;EACE,mBAAmB;EACnB,UAAU;AACZ;AACA;EACE,wBAAwB;AAC1B;AACA;EACE,wBAAwB;AAC1B;AACA;EACE,wBAAwB;AAC1B;AACA;EACE,yBAAyB;AAC3B;AACA;EACE,WAAW;EACX,WAAW;EACX,qFAAqF;EACrF,2BAA2B;AAC7B;AACA;EACE,aAAa;EACb,mBAAmB;EACnB,sBAAsB;AACxB;AACA;EACE,yBAAyB;EACzB,WAAW;AACb;AACA;EACE,WAAW;EACX,eAAe;EACf,iBAAiB;EACjB,OAAO;AACT;AACA;EACE,WAAW;AACb;AACA;EACE,aAAa;AACf;AACA;EACE,WAAW;AACb;AACA;EACE,aAAa;AACf;AACA;EACE,WAAW;AACb;AACA;EACE,WAAW;AACb;AACA;EACE,WAAW;AACb;AACA;EACE,WAAW;AACb;AACA;EACE,WAAW;AACb;AACA;EACE,WAAW;EACX,YAAY;AACd;AACA;EACE,aAAa;EACb,mBAAmB;AACrB;AACA;EACE,WAAW;EACX,eAAe;EACf,iBAAiB;EACjB,OAAO;AACT;AACA;EACE,yBAAyB;EACzB,kBAAkB;EAClB,kBAAkB;AACpB;AACA;EACE,WAAW;EACX,cAAc;EACd,eAAe;AACjB;AACA;EACE,cAAc;EACd,WAAW;EACX,WAAW;AACb;AACA;EACE,eAAe;AACjB;AACA;EACE,yBAAyB;EACzB,eAAe;EACf,YAAY;EACZ,kBAAkB;EAClB,aAAa;EACb,mBAAmB;AACrB;AACA;EACE,WAAW;EACX,cAAc;EACd,eAAe;AACjB;AACA;EACE,cAAc;EACd,WAAW;EACX,WAAW;AACb;AACA;EACE,eAAe;AACjB;AACA;EACE,yBAAyB;EACzB,eAAe;EACf,YAAY;EACZ,kBAAkB;EAClB,aAAa;EACb,mBAAmB;EACnB,uBAAuB;AACzB;AACA;EACE,eAAe;EACf,WAAW;EACX,eAAe;AACjB;AACA;EACE,eAAe;AACjB","file":"LinkAttr.vue","sourcesContent":[".link-attr-pattern-solid {\n  background: #2e6ee7;\n  width: 8px;\n}\n.link-attr-pattern-dashed_1_1 {\n  background-size: 1px 1px;\n}\n.link-attr-pattern-dashed_2_2 {\n  background-size: 2px 2px;\n}\n.link-attr-pattern-dashed_4_4 {\n  background-size: 4px 4px;\n}\n.link-attr-pattern-dashed_5_10 {\n  background-size: 5px 10px;\n}\n.link-attr-pattern {\n  width: 100%;\n  height: 1px;\n  background-image: linear-gradient(to right, #2e6ee7 0%, #2e6ee7 50%, transparent 50%);\n  background-repeat: repeat-x;\n}\n.link-attr-width-box {\n  display: flex;\n  align-items: center;\n  justify-content: right;\n}\n.link-attr-width {\n  background-color: #2e6ee7;\n  width: 20px;\n}\n.link-attr-width-label {\n  color: #666;\n  font-size: 12px;\n  margin-left: 10px;\n  flex: 1;\n}\n.link-attr-width-0_25 {\n  height: 2px;\n}\n.link-attr-width-0_75 {\n  height: 2.5px;\n}\n.link-attr-width-1 {\n  height: 3px;\n}\n.link-attr-width-1_5 {\n  height: 4.5px;\n}\n.link-attr-width-2 {\n  height: 5px;\n}\n.link-attr-width-3 {\n  height: 6px;\n}\n.link-attr-width-4 {\n  height: 7px;\n}\n.link-attr-width-5 {\n  height: 8px;\n}\n.link-attr-width-6 {\n  height: 9px;\n}\n.link-attr-type {\n  width: 14px;\n  height: 14px;\n}\n.link-attr-type-box {\n  display: flex;\n  align-items: center;\n}\n.link-attr-type-label {\n  color: #666;\n  font-size: 12px;\n  margin-left: 10px;\n  flex: 1;\n}\n.link-attr .link-attr-pattern-wrapper {\n  border: 1px solid #d9d9d9;\n  padding: 15px 10px;\n  border-radius: 4px;\n}\n.link-attr .link-attr-pattern-tricker {\n  width: 40px;\n  margin: 0 auto;\n  cursor: pointer;\n}\n.link-attr .link-attr-pattern-tricker::after {\n  display: block;\n  content: '';\n  height: 4px;\n}\n.link-attr .link-attr-pattern-tricker:hover {\n  cursor: pointer;\n}\n.link-attr .link-attr-width-wrapper {\n  border: 1px solid #d9d9d9;\n  padding: 0 10px;\n  height: 32px;\n  border-radius: 4px;\n  display: flex;\n  align-items: center;\n}\n.link-attr .link-attr-width-tricker {\n  width: 40px;\n  margin: 0 auto;\n  cursor: pointer;\n}\n.link-attr .link-attr-width-tricker::after {\n  display: block;\n  content: '';\n  height: 4px;\n}\n.link-attr .link-attr-width-tricker:hover {\n  cursor: pointer;\n}\n.link-attr .link-attr-type-wrapper {\n  border: 1px solid #d9d9d9;\n  padding: 0 10px;\n  height: 32px;\n  border-radius: 4px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-attr .link-attr-type-tricker {\n  font-size: 14px;\n  color: #666;\n  cursor: pointer;\n}\n.link-attr .link-attr-type-tricker:hover {\n  cursor: pointer;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$e = "data-v-7195079c";
  /* module identifier */
  const __vue_module_identifier__$e = undefined;
  /* functional template */
  const __vue_is_functional_template__$e = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$e = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$d, staticRenderFns: __vue_staticRenderFns__$d },
    __vue_inject_styles__$e,
    __vue_script__$e,
    __vue_scope_id__$e,
    __vue_is_functional_template__$e,
    __vue_module_identifier__$e,
    false,
    createInjector,
    undefined,
    undefined
  );

var expandSelects = [{
    value: "1",
    label: '是'
  }, {
    value: "0",
    label: '否'
  }],
  shapeSelects = [{
    value: 'rectangle',
    label: '长方形'
  }, {
    value: 'circle',
    label: '圆形'
  }, {
    value: 'roundrect',
    label: '圆角矩形'
  }];
var script$d = {
  name: 'GroupAttr',
  components: {
    BaseForm: __vue_component__$i,
    CommonSpace: __vue_component__$f,
    // ant design vue
    Form: antDesignVue.Form,
    FormItem: antDesignVue.Form.Item,
    Input: antDesignVue.Input,
    Select: antDesignVue.Select,
    Option: antDesignVue.Select.Option
  },
  props: {
    base: {
      type: Array,
      default: function _default() {
        return [{
          key: 'font',
          value: 13
        }, {
          key: 'color',
          value: '#000000'
        }, {
          key: 'position',
          value: 'bottom'
        }];
      }
    },
    group: {
      type: Array,
      default: function _default() {
        return [{
          key: 'expand',
          value: '1'
        }, {
          key: 'shape',
          value: 'rectangle'
        }, {
          key: 'fill',
          value: '#D6E0FF'
        }];
      }
    }
  },
  data: function data() {
    return {
      form: this.$form.createForm(this, {
        name: 'group'
      }),
      selects: {
        expand: expandSelects,
        shape: shapeSelects
      }
    };
  },
  watch: {
    group: {
      deep: true,
      immediate: true,
      handler: function handler(newValue) {
        var _this = this;
        this.$nextTick(function () {
          newValue.forEach(function (_ref) {
            var key = _ref.key,
              value = _ref.value;
            _this.form.setFieldsValue(_defineProperty({}, "".concat(key), value));
          });
        });
      }
    }
  }
};

/* script */
const __vue_script__$d = script$d;

/* template */
var __vue_render__$c = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "group-attr" },
    [
      _c("BaseForm", {
        attrs: { label: "请输入组名称", base: _vm.base },
        on: {
          label: function (value) {
            return _vm.$emit("groupLabel", value)
          },
          font: function (value) {
            return _vm.$emit("groupFont", value)
          },
          color: function (value) {
            return _vm.$emit("groupColor", value)
          },
          position: function (value) {
            return _vm.$emit("groupPosition", value)
          },
        },
      }),
      _vm._v(" "),
      _c(
        "CommonSpace",
        { attrs: { name: "组" } },
        [
          _c("Form", { attrs: { form: _vm.form, autocomplete: "off" } }, [
            _c(
              "div",
              { staticStyle: { display: "flex" } },
              [
                _c(
                  "FormItem",
                  {
                    staticStyle: { flex: "1" },
                    attrs: {
                      "label-col": { span: 8 },
                      "wrapper-col": { span: 12 },
                      label: "展开",
                    },
                  },
                  [
                    _c(
                      "Select",
                      {
                        directives: [
                          {
                            name: "decorator",
                            rawName: "v-decorator",
                            value: ["expand"],
                            expression: "['expand']",
                          },
                        ],
                        on: {
                          change: function (value) {
                            return _vm.$emit("groupExpand", value)
                          },
                        },
                      },
                      _vm._l(_vm.selects.expand, function (item) {
                        return _c(
                          "Option",
                          { key: item.value, attrs: { value: item.value } },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(item.label) +
                                "\n            "
                            ),
                          ]
                        )
                      }),
                      1
                    ),
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "FormItem",
                  {
                    staticStyle: {
                      flex: "1",
                      display: "flex",
                      "justify-content": "right",
                    },
                    attrs: {
                      "label-col": { span: 8 },
                      "wrapper-col": { span: 16 },
                      label: "形状",
                    },
                  },
                  [
                    _c(
                      "Select",
                      {
                        directives: [
                          {
                            name: "decorator",
                            rawName: "v-decorator",
                            value: ["shape"],
                            expression: "['shape']",
                          },
                        ],
                        on: {
                          change: function (value) {
                            return _vm.$emit("groupShape", value)
                          },
                        },
                      },
                      _vm._l(_vm.selects.shape, function (item) {
                        return _c(
                          "Option",
                          { key: item.value, attrs: { value: item.value } },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(item.label) +
                                "\n            "
                            ),
                          ]
                        )
                      }),
                      1
                    ),
                  ],
                  1
                ),
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticStyle: { display: "flex" } },
              [
                _c(
                  "FormItem",
                  {
                    staticStyle: { flex: "1" },
                    attrs: {
                      "label-col": { span: 8 },
                      "wrapper-col": { span: 12 },
                      label: "填充",
                    },
                  },
                  [
                    _c("Input", {
                      directives: [
                        {
                          name: "decorator",
                          rawName: "v-decorator",
                          value: ["fill"],
                          expression: "['fill']",
                        },
                      ],
                      attrs: { type: "color" },
                      nativeOn: {
                        change: function ($event) {
                          return function (e) {
                            return _vm.$emit("groupFill", e.target._value)
                          }.apply(null, arguments)
                        },
                      },
                    }),
                  ],
                  1
                ),
                _vm._v(" "),
                _c("FormItem", {
                  staticStyle: {
                    flex: "1",
                    display: "flex",
                    "justify-content": "right",
                  },
                  attrs: {
                    "label-col": { span: 8 },
                    "wrapper-col": { span: 12 },
                  },
                }),
              ],
              1
            ),
          ]),
        ],
        1
      ),
    ],
    1
  )
};
var __vue_staticRenderFns__$c = [];
__vue_render__$c._withStripped = true;

  /* style */
  const __vue_inject_styles__$d = undefined;
  /* scoped */
  const __vue_scope_id__$d = "data-v-13ac9fd2";
  /* module identifier */
  const __vue_module_identifier__$d = undefined;
  /* functional template */
  const __vue_is_functional_template__$d = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$d = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$c, staticRenderFns: __vue_staticRenderFns__$c },
    __vue_inject_styles__$d,
    __vue_script__$d,
    __vue_scope_id__$d,
    __vue_is_functional_template__$d,
    __vue_module_identifier__$d,
    false,
    undefined,
    undefined,
    undefined
  );

__vue_component__$g.install = function (Vue) {
  Vue.component(__vue_component__$g.name, __vue_component__$g);
};
__vue_component__$h.install = function (Vue) {
  Vue.component(__vue_component__$h.name, __vue_component__$h);
};
__vue_component__$d.install = function (Vue) {
  Vue.component(__vue_component__$d.name, __vue_component__$d);
};
__vue_component__$e.install = function (Vue) {
  Vue.component(__vue_component__$e.name, __vue_component__$e);
};
__vue_component__$j.install = function (Vue) {
  Vue.component(__vue_component__$j.name, __vue_component__$j);
};

//
//
//
//
//
//

var script$c = {
  name: 'Canvas',
  props: {},
  methods: {
    handleDrop: function handleDrop(e) {
      e.preventDefault();
    }
  }
};

/* script */
const __vue_script__$c = script$c;

/* template */
var __vue_render__$b = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    {
      staticClass: "canvas",
      on: {
        drop: _vm.handleDrop,
        dragover: function (e) {
          return e.preventDefault()
        },
      },
    },
    [_vm._t("default")],
    2
  )
};
var __vue_staticRenderFns__$b = [];
__vue_render__$b._withStripped = true;

  /* style */
  const __vue_inject_styles__$c = function (inject) {
    if (!inject) return
    inject("data-v-ed73f71e_0", { source: ".canvas {\n  width: 100%;\n  height: 100%;\n  background-color: inherit;\n}\n", map: {"version":3,"sources":["Canvas.vue"],"names":[],"mappings":"AAAA;EACE,WAAW;EACX,YAAY;EACZ,yBAAyB;AAC3B","file":"Canvas.vue","sourcesContent":[".canvas {\n  width: 100%;\n  height: 100%;\n  background-color: inherit;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$c = undefined;
  /* module identifier */
  const __vue_module_identifier__$c = undefined;
  /* functional template */
  const __vue_is_functional_template__$c = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$c = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$b, staticRenderFns: __vue_staticRenderFns__$b },
    __vue_inject_styles__$c,
    __vue_script__$c,
    __vue_scope_id__$c,
    __vue_is_functional_template__$c,
    __vue_module_identifier__$c,
    false,
    createInjector,
    undefined,
    undefined
  );

__vue_component__$c.install = function (Vue) {
  Vue.component(__vue_component__$c.name, __vue_component__$c);
};

//
//
//
//
//
//

var script$b = {
  name: 'Data'
};

/* script */
const __vue_script__$b = script$b;

/* template */
var __vue_render__$a = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("div", { staticClass: "data" }, [_vm._t("default")], 2)
};
var __vue_staticRenderFns__$a = [];
__vue_render__$a._withStripped = true;

  /* style */
  const __vue_inject_styles__$b = function (inject) {
    if (!inject) return
    inject("data-v-12db76f4_0", { source: ".data[data-v-12db76f4] {\n  padding: 0 20px;\n}\n", map: {"version":3,"sources":["Data.vue"],"names":[],"mappings":"AAAA;EACE,eAAe;AACjB","file":"Data.vue","sourcesContent":[".data {\n  padding: 0 20px;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$b = "data-v-12db76f4";
  /* module identifier */
  const __vue_module_identifier__$b = undefined;
  /* functional template */
  const __vue_is_functional_template__$b = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$b = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$a, staticRenderFns: __vue_staticRenderFns__$a },
    __vue_inject_styles__$b,
    __vue_script__$b,
    __vue_scope_id__$b,
    __vue_is_functional_template__$b,
    __vue_module_identifier__$b,
    false,
    createInjector,
    undefined,
    undefined
  );

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;
  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
  return arr2;
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}

//
var script$a = {
  name: 'LinkDataListItem',
  props: {
    icon: {
      type: String,
      default: ''
    },
    label: {
      type: String,
      default: ''
    },
    value: {
      type: String,
      default: ''
    }
  },
  components: {
    Avatar: antDesignVue.Avatar,
    Icon: antDesignVue.Icon,
    Tooltip: antDesignVue.Tooltip
  },
  data: function data() {
    return {};
  },
  methods: {
    handleDelete: function handleDelete() {
      this.$emit('delete', this.value);
    }
  }
};

/* script */
const __vue_script__$a = script$a;

/* template */
var __vue_render__$9 = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("div", { staticClass: "link-datalist-item" }, [
    _c(
      "div",
      { staticClass: "link-datalist-item-left" },
      [
        _c("Avatar", {
          staticClass: "link-datalist-item-icon",
          attrs: { src: _vm.icon },
        }),
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "link-datalist-item-middle" },
      [
        _c("Tooltip", { attrs: { title: _vm.label } }, [
          _c("span", { staticClass: "link-datalist-item-label" }, [
            _vm._v(_vm._s(_vm.label)),
          ]),
        ]),
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "link-datalist-item-right" },
      [
        _c("Icon", {
          staticClass: "link-datalist-item-button",
          attrs: { type: "delete" },
          on: { click: _vm.handleDelete },
        }),
      ],
      1
    ),
  ])
};
var __vue_staticRenderFns__$9 = [];
__vue_render__$9._withStripped = true;

  /* style */
  const __vue_inject_styles__$a = function (inject) {
    if (!inject) return
    inject("data-v-4c55a434_0", { source: ".link-datalist-item[data-v-4c55a434] {\n  width: 100%;\n  border: 1px solid #fff;\n  padding: 10px 0;\n  margin-bottom: 10px;\n  background: linear-gradient(180deg, #edf4ff 0%, #ffffff 64%, #deedff 100%);\n  box-shadow: 0px 2px 6px 2px rgba(0, 0, 0, 0.05);\n  display: flex;\n}\n.link-datalist-item .link-datalist-item-left[data-v-4c55a434] {\n  flex: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-datalist-item .link-datalist-item-left .link-datalist-item-icon[data-v-4c55a434] {\n  width: 24px;\n  height: 24px;\n}\n.link-datalist-item .link-datalist-item-middle[data-v-4c55a434] {\n  flex: 3;\n  display: flex;\n  align-items: center;\n  justify-content: left;\n}\n.link-datalist-item .link-datalist-item-middle .link-datalist-item-label[data-v-4c55a434] {\n  font-size: 14px;\n  font-size: bold;\n  color: #333;\n  line-height: 32px;\n  max-width: 120px;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.link-datalist-item .link-datalist-item-right[data-v-4c55a434] {\n  flex: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-datalist-item .link-datalist-item-right .link-datalist-item-button[data-v-4c55a434] {\n  font-size: 14px;\n  color: #2E6EE7;\n}\n", map: {"version":3,"sources":["LinkDataListItem.vue"],"names":[],"mappings":"AAAA;EACE,WAAW;EACX,sBAAsB;EACtB,eAAe;EACf,mBAAmB;EACnB,0EAA0E;EAC1E,+CAA+C;EAC/C,aAAa;AACf;AACA;EACE,OAAO;EACP,aAAa;EACb,mBAAmB;EACnB,uBAAuB;AACzB;AACA;EACE,WAAW;EACX,YAAY;AACd;AACA;EACE,OAAO;EACP,aAAa;EACb,mBAAmB;EACnB,qBAAqB;AACvB;AACA;EACE,eAAe;EACf,eAAe;EACf,WAAW;EACX,iBAAiB;EACjB,gBAAgB;EAChB,mBAAmB;EACnB,gBAAgB;EAChB,uBAAuB;AACzB;AACA;EACE,OAAO;EACP,aAAa;EACb,mBAAmB;EACnB,uBAAuB;AACzB;AACA;EACE,eAAe;EACf,cAAc;AAChB","file":"LinkDataListItem.vue","sourcesContent":[".link-datalist-item {\n  width: 100%;\n  border: 1px solid #fff;\n  padding: 10px 0;\n  margin-bottom: 10px;\n  background: linear-gradient(180deg, #edf4ff 0%, #ffffff 64%, #deedff 100%);\n  box-shadow: 0px 2px 6px 2px rgba(0, 0, 0, 0.05);\n  display: flex;\n}\n.link-datalist-item .link-datalist-item-left {\n  flex: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-datalist-item .link-datalist-item-left .link-datalist-item-icon {\n  width: 24px;\n  height: 24px;\n}\n.link-datalist-item .link-datalist-item-middle {\n  flex: 3;\n  display: flex;\n  align-items: center;\n  justify-content: left;\n}\n.link-datalist-item .link-datalist-item-middle .link-datalist-item-label {\n  font-size: 14px;\n  font-size: bold;\n  color: #333;\n  line-height: 32px;\n  max-width: 120px;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.link-datalist-item .link-datalist-item-right {\n  flex: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-datalist-item .link-datalist-item-right .link-datalist-item-button {\n  font-size: 14px;\n  color: #2E6EE7;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$a = "data-v-4c55a434";
  /* module identifier */
  const __vue_module_identifier__$a = undefined;
  /* functional template */
  const __vue_is_functional_template__$a = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$a = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$9, staticRenderFns: __vue_staticRenderFns__$9 },
    __vue_inject_styles__$a,
    __vue_script__$a,
    __vue_scope_id__$a,
    __vue_is_functional_template__$a,
    __vue_module_identifier__$a,
    false,
    createInjector,
    undefined,
    undefined
  );

var script$9 = {
  name: 'LinkData',
  components: {
    LinkDataListItem: __vue_component__$a,
    // ant design vue
    Select: antDesignVue.Select,
    Option: antDesignVue.Select.Option,
    Checkbox: antDesignVue.Checkbox,
    CheckboxGroup: antDesignVue.Checkbox.Group,
    Button: antDesignVue.Button,
    ButtonGroup: antDesignVue.Button.Group,
    Spin: antDesignVue.Spin,
    Tooltip: antDesignVue.Tooltip
  },
  props: {
    selects: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    // 已有数据
    list: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    listValueMap: {
      type: Object,
      default: function _default() {}
    },
    dataSourceLoading: {
      type: Boolean,
      default: false
    },
    dataListLoading: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      isAdd: true,
      checkList: [],
      dataList: [],
      open: false,
      isClickOutside: true
    };
  },
  methods: {
    handleAdd: function handleAdd() {
      this.isAdd = false;
    },
    handleDeselect: function handleDeselect(value, option, checkList) {
      var idx = this.checkList.indexOf(value.key);
      idx != -1 && this.checkList.splice(idx, 1);
    },
    handleChange: function handleChange(e, value) {
      if (e.target.checked) {
        !this.checkList.includes(value) && this.checkList.push(value);
      } else {
        var idx = this.checkList.indexOf(value);
        idx != -1 && this.checkList.splice(idx, 1);
      }
    },
    handleFocus: function handleFocus(value) {
      this.open = true;
    },
    handleBlur: function handleBlur(value) {
      if (this.isClickOutside) {
        this.open = false;
      } else {
        this.open = true;
      }
    },
    handleOpen: function handleOpen() {
      this.open = false;
    },
    handleCheckAllChange: function handleCheckAllChange(e) {
      var _this = this;
      this.checkList = e.target.checked ? this.selects.map(function (m) {
        return m.value;
      }).filter(function (f) {
        return !_this.dataList.includes(f);
      }) : [];
    },
    handleConfirm: function handleConfirm() {
      this.$emit('change', this.dataList.concat(this.checkList));
    },
    handleCancel: function handleCancel() {
      this.isAdd = true;
      this.dataList = _toConsumableArray(this.list || []);
      this.checkList = [];
    },
    handleDelete: function handleDelete(value) {
      var idx = this.dataList.indexOf(value);
      var tmp = _toConsumableArray(this.dataList);
      tmp.splice(idx, 1);
      this.$emit('change', tmp);
    },
    handleScroll: function handleScroll() {
      this.$emit('linkScroll', this.$refs.scrollContainer);
    },
    handleDropdownVisibleChange: function handleDropdownVisibleChange(open) {
      this.open = open;
    },
    handleClickOutside: function handleClickOutside(e) {
      var inputSelect = document.getElementById('inputSelect'),
        dropdownSelect = document.getElementById('dropdownSelect');
      if (this.open) {
        if (inputSelect && dropdownSelect) {
          this.isClickOutside = !(inputSelect.contains(e.target) || dropdownSelect.contains(e.target));
          if (!this.isClickOutside) {
            dropdownSelect.style.display = 'block';
            if (e.target.id == 'open') {
              this.handleOpen();
            } else {
              this.open = true;
            }
          } else {
            dropdownSelect.style.display = 'none';
            this.open = false;
          }
        }
      }
    }
  },
  watch: {
    list: {
      immediate: true,
      deep: true,
      handler: function handler(newValue, oldValue) {
        this.dataList = _toConsumableArray(newValue || []);
        this.checkList = [];
      }
    }
  },
  mounted: function mounted() {
    // if (this.list.length > 0) {
    //   this.isAdd = false
    // }
    window.addEventListener('click', this.handleClickOutside, false);
  },
  destroyed: function destroyed() {
    window.removeEventListener('click', this.handleClickOutside, false);
  },
  computed: {
    computeCheckList: function computeCheckList() {
      return function (checkList) {
        var _this2 = this;
        return checkList.map(function (m) {
          return {
            key: m,
            label: _this2.listValueMap["".concat(m)]
          };
        });
      };
    }
  }
};

/* script */
const __vue_script__$9 = script$9;

/* template */
var __vue_render__$8 = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("div", { staticClass: "link-data" }, [
    _c("div", { staticClass: "link-data-operate" }, [
      _vm.isAdd
        ? _c(
            "div",
            { staticClass: "link-data-operate-add" },
            [
              _c(
                "Button",
                { attrs: { type: "primary" }, on: { click: _vm.handleAdd } },
                [_vm._v("添加数据")]
              ),
            ],
            1
          )
        : _c(
            "div",
            { staticClass: "link-data-operate-select" },
            [
              _c(
                "Select",
                {
                  staticStyle: { width: "100%" },
                  attrs: {
                    value: _vm.computeCheckList(_vm.checkList),
                    maxTagTextLength: 3,
                    id: "inputSelect",
                    labelInValue: "",
                    maxTagCount: 2,
                    showArrow: "",
                    mode: "multiple",
                    open: _vm.open,
                  },
                  on: {
                    focus: _vm.handleFocus,
                    blur: _vm.handleBlur,
                    dropdownVisibleChange: _vm.handleDropdownVisibleChange,
                    deselect: function (value, option) {
                      return _vm.handleDeselect(value, option, _vm.checkList)
                    },
                  },
                },
                [
                  _c(
                    "div",
                    {
                      staticStyle: { padding: "10px", width: "100%" },
                      attrs: { slot: "dropdownRender", id: "dropdownSelect" },
                      slot: "dropdownRender",
                    },
                    [
                      _c(
                        "Spin",
                        {
                          attrs: {
                            spinning: _vm.dataSourceLoading,
                            tip: "Loading...",
                          },
                        },
                        [
                          _c(
                            "div",
                            {
                              staticStyle: {
                                display: "flex",
                                "align-items": "center",
                                "justify-content": "space-between",
                                "padding-bottom": "16px",
                                "border-bottom": "1px dashed #ccc",
                              },
                            },
                            [
                              _c(
                                "Checkbox",
                                {
                                  attrs: {
                                    checked:
                                      _vm.checkList.length ==
                                      _vm.selects.length,
                                  },
                                  on: { change: _vm.handleCheckAllChange },
                                },
                                [
                                  _vm._v(
                                    "\n                全选\n              "
                                  ),
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "Button",
                                {
                                  attrs: {
                                    size: "small",
                                    type: "primary",
                                    id: "open",
                                  },
                                  on: { click: _vm.handleOpen },
                                },
                                [
                                  _vm._v(
                                    "\n                确定\n              "
                                  ),
                                ]
                              ),
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              ref: "scrollContainer",
                              staticStyle: {
                                "margin-top": "10px",
                                "max-height": "160px",
                                "overflow-y": "auto",
                              },
                              on: { scroll: _vm.handleScroll },
                            },
                            _vm._l(_vm.selects, function (item) {
                              return _c(
                                "div",
                                {
                                  key: item.value,
                                  staticStyle: { margin: "0", padding: "0" },
                                },
                                [
                                  _c(
                                    "Checkbox",
                                    {
                                      staticStyle: {
                                        width: "100%",
                                        display: "flex",
                                        "align-items": "center",
                                      },
                                      attrs: {
                                        checked: _vm.checkList.includes(
                                          item.value
                                        ),
                                        disabled: _vm.dataList.includes(
                                          item.value
                                        ),
                                      },
                                      on: {
                                        change: function (e) {
                                          return _vm.handleChange(e, item.value)
                                        },
                                      },
                                    },
                                    [
                                      _c(
                                        "Tooltip",
                                        { attrs: { title: item.label } },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticStyle: {
                                                "max-width": "120px",
                                                "white-space": "nowrap",
                                                overflow: "hidden",
                                                "text-overflow": "ellipsis",
                                              },
                                            },
                                            [
                                              _vm._v(
                                                "\n                      " +
                                                  _vm._s(item.label) +
                                                  "\n                    "
                                              ),
                                            ]
                                          ),
                                        ]
                                      ),
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("br"),
                                ],
                                1
                              )
                            }),
                            0
                          ),
                        ]
                      ),
                    ],
                    1
                  ),
                ]
              ),
              _vm._v(" "),
              _c(
                "ButtonGroup",
                { staticClass: "link-data-operate-select-button_group" },
                [
                  _c(
                    "Button",
                    {
                      attrs: { type: "primary" },
                      on: { click: _vm.handleConfirm },
                    },
                    [_vm._v("确定")]
                  ),
                  _vm._v(" "),
                  _c("Button", { on: { click: _vm.handleCancel } }, [
                    _vm._v("取消"),
                  ]),
                ],
                1
              ),
            ],
            1
          ),
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "link-data-list" },
      [
        _vm.dataList.length > 0
          ? [
              _c(
                "Spin",
                { attrs: { spinning: _vm.dataListLoading, tip: "Loading..." } },
                _vm._l(_vm.computeCheckList(_vm.dataList), function (item) {
                  return _c("LinkDataListItem", {
                    key: item.key,
                    attrs: {
                      label: item.label,
                      value: item.key,
                      icon: "http://10.186.2.55:8148/tgcos/582717604620:vwaver/icon-datasource.svg",
                    },
                    on: { delete: _vm.handleDelete },
                  })
                }),
                1
              ),
            ]
          : [
              _c("span", { staticClass: "link-data-list-tip" }, [
                _vm._v(
                  "\n        点击添加数据按钮，添加关联的电路信息\n      "
                ),
              ]),
            ],
      ],
      2
    ),
  ])
};
var __vue_staticRenderFns__$8 = [];
__vue_render__$8._withStripped = true;

  /* style */
  const __vue_inject_styles__$9 = function (inject) {
    if (!inject) return
    inject("data-v-082ddf42_0", { source: ".link-data .link-data-operate[data-v-082ddf42] {\n  min-height: 100px;\n  border-bottom: 1px dashed #ececec;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-data .link-data-operate .link-data-operate-add[data-v-082ddf42] {\n  margin-top: -20px;\n  flex: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-data .link-data-operate .link-data-operate-select[data-v-082ddf42] {\n  flex: 1;\n}\n.link-data .link-data-operate .link-data-operate-select .link-data-operate-select_option[data-v-082ddf42] {\n  padding: 10px;\n}\n.link-data .link-data-operate .link-data-operate-select .link-data-operate-select-button_group[data-v-082ddf42] {\n  margin: 12px 0;\n  display: flex;\n  justify-content: right;\n}\n.link-data .link-data-list[data-v-082ddf42] {\n  margin-top: 10px;\n  text-align: center;\n  max-height: calc(90vh - 260px);\n  overflow-y: auto;\n}\n.link-data .link-data-list .link-data-list-tip[data-v-082ddf42] {\n  color: #999;\n  font-size: 12px;\n}\n", map: {"version":3,"sources":["LinkData.vue"],"names":[],"mappings":"AAAA;EACE,iBAAiB;EACjB,iCAAiC;EACjC,aAAa;EACb,mBAAmB;EACnB,uBAAuB;AACzB;AACA;EACE,iBAAiB;EACjB,OAAO;EACP,aAAa;EACb,mBAAmB;EACnB,uBAAuB;AACzB;AACA;EACE,OAAO;AACT;AACA;EACE,aAAa;AACf;AACA;EACE,cAAc;EACd,aAAa;EACb,sBAAsB;AACxB;AACA;EACE,gBAAgB;EAChB,kBAAkB;EAClB,8BAA8B;EAC9B,gBAAgB;AAClB;AACA;EACE,WAAW;EACX,eAAe;AACjB","file":"LinkData.vue","sourcesContent":[".link-data .link-data-operate {\n  min-height: 100px;\n  border-bottom: 1px dashed #ececec;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-data .link-data-operate .link-data-operate-add {\n  margin-top: -20px;\n  flex: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.link-data .link-data-operate .link-data-operate-select {\n  flex: 1;\n}\n.link-data .link-data-operate .link-data-operate-select .link-data-operate-select_option {\n  padding: 10px;\n}\n.link-data .link-data-operate .link-data-operate-select .link-data-operate-select-button_group {\n  margin: 12px 0;\n  display: flex;\n  justify-content: right;\n}\n.link-data .link-data-list {\n  margin-top: 10px;\n  text-align: center;\n  max-height: calc(90vh - 260px);\n  overflow-y: auto;\n}\n.link-data .link-data-list .link-data-list-tip {\n  color: #999;\n  font-size: 12px;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$9 = "data-v-082ddf42";
  /* module identifier */
  const __vue_module_identifier__$9 = undefined;
  /* functional template */
  const __vue_is_functional_template__$9 = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$9 = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$8, staticRenderFns: __vue_staticRenderFns__$8 },
    __vue_inject_styles__$9,
    __vue_script__$9,
    __vue_scope_id__$9,
    __vue_is_functional_template__$9,
    __vue_module_identifier__$9,
    false,
    createInjector,
    undefined,
    undefined
  );

var script$8 = {
  name: 'NodeData',
  props: {
    node: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  components: {
    Form: antDesignVue.Form,
    FormItem: antDesignVue.Form.Item,
    Input: antDesignVue.Input
  },
  data: function data() {
    return {
      form: this.$form.createForm(this, {
        name: 'node'
      })
    };
  },
  mounted: function mounted() {
    var _this = this;
    this.node.forEach(function (_ref) {
      var key = _ref.key,
        value = _ref.value;
      _this.form.setFieldsValue(_defineProperty({}, "".concat(key), value));
    });
  }
};

/* script */
const __vue_script__$8 = script$8;

/* template */
var __vue_render__$7 = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "node-data" },
    [
      _c(
        "Form",
        {
          attrs: {
            form: _vm.form,
            "label-col": { span: 4 },
            "wrapper-col": { span: 20 },
            labelAlign: "right",
            autocomplete: "off",
          },
        },
        [
          _c(
            "FormItem",
            { attrs: { label: "区域" } },
            [
              _c("Input", {
                directives: [
                  {
                    name: "decorator",
                    rawName: "v-decorator",
                    value: ["area"],
                    expression: "[\n          'area'\n        ]",
                  },
                ],
                attrs: { disabled: true },
              }),
            ],
            1
          ),
        ],
        1
      ),
    ],
    1
  )
};
var __vue_staticRenderFns__$7 = [];
__vue_render__$7._withStripped = true;

  /* style */
  const __vue_inject_styles__$8 = undefined;
  /* scoped */
  const __vue_scope_id__$8 = "data-v-670653c2";
  /* module identifier */
  const __vue_module_identifier__$8 = undefined;
  /* functional template */
  const __vue_is_functional_template__$8 = false;
  /* style inject */
  
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$8 = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$7, staticRenderFns: __vue_staticRenderFns__$7 },
    __vue_inject_styles__$8,
    __vue_script__$8,
    __vue_scope_id__$8,
    __vue_is_functional_template__$8,
    __vue_module_identifier__$8,
    false,
    undefined,
    undefined,
    undefined
  );

__vue_component__$9.install = function (Vue) {
  Vue.component(__vue_component__$9.name, __vue_component__$9);
};
__vue_component__$8.install = function (Vue) {
  Vue.component(__vue_component__$8.name, __vue_component__$8);
};
__vue_component__$b.install = function (Vue) {
  Vue.component(__vue_component__$b.name, __vue_component__$b);
};

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArrayLimit(arr, i) {
  var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"];
  if (null != _i) {
    var _s,
      _e,
      _x,
      _r,
      _arr = [],
      _n = !0,
      _d = !1;
    try {
      if (_x = (_i = _i.call(arr)).next, 0 === i) {
        if (Object(_i) !== _i) return;
        _n = !1;
      } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0);
    } catch (err) {
      _d = !0, _e = err;
    } finally {
      try {
        if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return;
      } finally {
        if (_d) throw _e;
      }
    }
    return _arr;
  }
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}

//
//
//
//
//
//
//
//
//

var script$7 = {
  name: 'LibraryBox',
  props: {
    icon: {
      type: String,
      default: ''
    },
    title: {
      type: String,
      default: ''
    }
  },
  computed: {
    computeAlt: function computeAlt() {
      return function (icon) {
        var iconFile = icon.split('/').at(-1);
        var _iconFile$split = iconFile.split('.'),
          _iconFile$split2 = _slicedToArray(_iconFile$split, 2),
          fileName = _iconFile$split2[0];
          _iconFile$split2[1];
        return fileName;
      };
    }
  }
};

/* script */
const __vue_script__$7 = script$7;

/* template */
var __vue_render__$6 = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("div", { staticClass: "library-box" }, [
    _c("img", {
      staticClass: "library-box-icon",
      attrs: { src: _vm.icon, alt: _vm.computeAlt(_vm.icon) },
    }),
    _vm._v(" "),
    _c("div", { staticClass: "library-box-title" }, [
      _c("span", [_vm._v(_vm._s(_vm.title))]),
    ]),
  ])
};
var __vue_staticRenderFns__$6 = [];
__vue_render__$6._withStripped = true;

  /* style */
  const __vue_inject_styles__$7 = function (inject) {
    if (!inject) return
    inject("data-v-2cb02c91_0", { source: ".library-box[data-v-2cb02c91] {\n  text-align: center;\n  width: 100%;\n}\n.library-box .library-box-icon[data-v-2cb02c91] {\n  width: 64px;\n  min-width: 64px;\n}\n.library-box .library-box-icon[data-v-2cb02c91]:hover {\n  background: rgba(46, 110, 231, 0.16);\n  border-radius: 4px;\n}\n.library-box .library-box-title[data-v-2cb02c91] {\n  color: #666;\n  font-size: 12px;\n  margin-top: 6px;\n}\n", map: {"version":3,"sources":["LibraryBox.vue"],"names":[],"mappings":"AAAA;EACE,kBAAkB;EAClB,WAAW;AACb;AACA;EACE,WAAW;EACX,eAAe;AACjB;AACA;EACE,oCAAoC;EACpC,kBAAkB;AACpB;AACA;EACE,WAAW;EACX,eAAe;EACf,eAAe;AACjB","file":"LibraryBox.vue","sourcesContent":[".library-box {\n  text-align: center;\n  width: 100%;\n}\n.library-box .library-box-icon {\n  width: 64px;\n  min-width: 64px;\n}\n.library-box .library-box-icon:hover {\n  background: rgba(46, 110, 231, 0.16);\n  border-radius: 4px;\n}\n.library-box .library-box-title {\n  color: #666;\n  font-size: 12px;\n  margin-top: 6px;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$7 = "data-v-2cb02c91";
  /* module identifier */
  const __vue_module_identifier__$7 = undefined;
  /* functional template */
  const __vue_is_functional_template__$7 = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$7 = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$6, staticRenderFns: __vue_staticRenderFns__$6 },
    __vue_inject_styles__$7,
    __vue_script__$7,
    __vue_scope_id__$7,
    __vue_is_functional_template__$7,
    __vue_module_identifier__$7,
    false,
    createInjector,
    undefined,
    undefined
  );

//
//
//
//
//
//
//
//
//

var script$6 = {
  name: 'LibraryBoxGroup',
  props: {
    name: {
      type: String,
      default: ''
    }
  }
};

/* script */
const __vue_script__$6 = script$6;

/* template */
var __vue_render__$5 = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "library-box-group" },
    [
      _c("div", { staticClass: "group-header" }, [
        _c("span", { staticClass: "group-header-name" }, [
          _vm._v(_vm._s(_vm.name)),
        ]),
      ]),
      _vm._v(" "),
      _vm._t("element"),
    ],
    2
  )
};
var __vue_staticRenderFns__$5 = [];
__vue_render__$5._withStripped = true;

  /* style */
  const __vue_inject_styles__$6 = function (inject) {
    if (!inject) return
    inject("data-v-c23b594e_0", { source: ".library-box-group[data-v-c23b594e] {\n  display: flex;\n  flex-direction: column;\n  padding: 0 10px 10px;\n}\n.library-box-group .group-header[data-v-c23b594e] {\n  display: flex;\n  align-items: center;\n  margin-bottom: 8px;\n}\n.library-box-group .group-header .group-header-name[data-v-c23b594e] {\n  color: #333;\n  margin-left: 6px;\n  font-size: 14px;\n}\n.library-box-group .group-header[data-v-c23b594e]::before {\n  content: '';\n  display: block;\n  width: 3px;\n  height: 14px;\n  background-color: #2E6EE7;\n}\n", map: {"version":3,"sources":["LibraryBoxGroup.vue"],"names":[],"mappings":"AAAA;EACE,aAAa;EACb,sBAAsB;EACtB,oBAAoB;AACtB;AACA;EACE,aAAa;EACb,mBAAmB;EACnB,kBAAkB;AACpB;AACA;EACE,WAAW;EACX,gBAAgB;EAChB,eAAe;AACjB;AACA;EACE,WAAW;EACX,cAAc;EACd,UAAU;EACV,YAAY;EACZ,yBAAyB;AAC3B","file":"LibraryBoxGroup.vue","sourcesContent":[".library-box-group {\n  display: flex;\n  flex-direction: column;\n  padding: 0 10px 10px;\n}\n.library-box-group .group-header {\n  display: flex;\n  align-items: center;\n  margin-bottom: 8px;\n}\n.library-box-group .group-header .group-header-name {\n  color: #333;\n  margin-left: 6px;\n  font-size: 14px;\n}\n.library-box-group .group-header::before {\n  content: '';\n  display: block;\n  width: 3px;\n  height: 14px;\n  background-color: #2E6EE7;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$6 = "data-v-c23b594e";
  /* module identifier */
  const __vue_module_identifier__$6 = undefined;
  /* functional template */
  const __vue_is_functional_template__$6 = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$6 = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$5, staticRenderFns: __vue_staticRenderFns__$5 },
    __vue_inject_styles__$6,
    __vue_script__$6,
    __vue_scope_id__$6,
    __vue_is_functional_template__$6,
    __vue_module_identifier__$6,
    false,
    createInjector,
    undefined,
    undefined
  );

//
var script$5 = {
  name: 'Library',
  props: {
    selects: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    selectedValue: {
      require: true
    }
  },
  components: {
    Select: antDesignVue.Select,
    Option: antDesignVue.Select.Option
  }
};

/* script */
const __vue_script__$5 = script$5;

/* template */
var __vue_render__$4 = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "library" },
    [
      _c(
        "Select",
        {
          staticClass: "library-select",
          attrs: { value: _vm.selectedValue },
          on: {
            change: function (value) {
              return _vm.$emit("library", value)
            },
          },
        },
        _vm._l(_vm.selects, function (item) {
          return _c(
            "Option",
            { key: item.value, attrs: { value: item.value } },
            [_vm._v(_vm._s(item.label))]
          )
        }),
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "library-content" }, [_vm._t("default")], 2),
    ],
    1
  )
};
var __vue_staticRenderFns__$4 = [];
__vue_render__$4._withStripped = true;

  /* style */
  const __vue_inject_styles__$5 = function (inject) {
    if (!inject) return
    inject("data-v-6b4460c6_0", { source: ".library {\n  width: 100%;\n}\n.library .library-select {\n  margin-top: 16px;\n  padding: 0 10px;\n  width: 100%;\n}\n.library .library-content {\n  margin-top: 16px;\n  max-height: calc(50vh - 100px);\n  overflow-y: auto;\n}\n", map: {"version":3,"sources":["Library.vue"],"names":[],"mappings":"AAAA;EACE,WAAW;AACb;AACA;EACE,gBAAgB;EAChB,eAAe;EACf,WAAW;AACb;AACA;EACE,gBAAgB;EAChB,8BAA8B;EAC9B,gBAAgB;AAClB","file":"Library.vue","sourcesContent":[".library {\n  width: 100%;\n}\n.library .library-select {\n  margin-top: 16px;\n  padding: 0 10px;\n  width: 100%;\n}\n.library .library-content {\n  margin-top: 16px;\n  max-height: calc(50vh - 100px);\n  overflow-y: auto;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$5 = undefined;
  /* module identifier */
  const __vue_module_identifier__$5 = undefined;
  /* functional template */
  const __vue_is_functional_template__$5 = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$5 = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$4, staticRenderFns: __vue_staticRenderFns__$4 },
    __vue_inject_styles__$5,
    __vue_script__$5,
    __vue_scope_id__$5,
    __vue_is_functional_template__$5,
    __vue_module_identifier__$5,
    false,
    createInjector,
    undefined,
    undefined
  );

__vue_component__$5.install = function (Vue) {
  Vue.component(__vue_component__$5.name, __vue_component__$5);
};
__vue_component__$7.install = function (Vue) {
  Vue.component(__vue_component__$7.name, __vue_component__$7);
};
__vue_component__$6.install = function (Vue) {
  Vue.component(__vue_component__$6.name, __vue_component__$6);
};

// 获取父节点的key值
var getParentKey$1 = function getParentKey(key, tree) {
  var parentKey;
  for (var i = 0; i < tree.length; i++) {
    var node = tree[i];
    if (node.children) {
      if (node.children.some(function (item) {
        return item.key === key;
      })) {
        parentKey = node.key;
      } else if (getParentKey(key, node.children)) {
        parentKey = getParentKey(key, node.children);
      }
    }
  }
  return parentKey;
};

// 获取叶子节点的key值
var getNodeKeys$1 = function getNodeKeys(nodeKeys, tree) {
  for (var i = 0; i < tree.length; i++) {
    var node = tree[i];
    if (node.children) {
      getNodeKeys(nodeKeys, node.children);
    } else {
      nodeKeys.push(node.key);
    }
  }
  return nodeKeys;
};
var script$4 = {
  name: 'Resource',
  props: {
    treeData: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    dragKeys: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  components: {
    TreeNode: antDesignVue.Tree.TreeNode,
    InputSearch: antDesignVue.Input.Search,
    Tree: antDesignVue.Tree,
    Tooltip: antDesignVue.Tooltip
  },
  data: function data() {
    return {
      index: 0,
      expandedKeys: [],
      matchedKeys: [],
      searchValue: '',
      autoExpandParent: true
    };
  },
  methods: {
    handleChange: function handleChange(e) {
      var _this = this;
      this.searchValue = e.target.value;
      this.autoExpandParent = true;
      // 拍平处理
      var dataList = [];
      var generateList = function generateList(data) {
        for (var i = 0; i < data.length; i++) {
          var node = data[i];
          var key = node.key,
            title = node.title;
          dataList.push({
            key: key,
            title: title
          });
          if (node.children) {
            generateList(node.children);
          }
        }
      };
      generateList(this.treeData);
      this.expandedKeys = dataList.map(function (item) {
        if (item.title.indexOf(e.target.value) > -1) {
          return getParentKey$1(item.key, _this.treeData);
        }
        return null;
      }).filter(function (item, i, self) {
        return item && self.indexOf(item) === i;
      });
      if (this.searchValue) {
        this.matchedKeys = dataList.filter(function (f) {
          return f.title.indexOf(_this.searchValue) > -1;
        }).map(function (m) {
          return m.key;
        });
        this.handleScrollIntoView(0);
      } else {
        if (document.getElementById("resource-tree")) {
          document.getElementById("resource-tree").scrollTop = 0;
        }
      }
    },
    handleScrollIntoView: function handleScrollIntoView(i) {
      var _this2 = this;
      if (this.matchedKeys.length > 0) {
        this.$nextTick(function () {
          var target = document.getElementById("".concat(_this2.matchedKeys[i % _this2.matchedKeys.length]));
          if (target) target.scrollIntoView();
        });
      }
    },
    handlePressEnter: function handlePressEnter(e) {
      if (e.target.value) {
        this.handleScrollIntoView(++this.index);
      }
    },
    handleExpand: function handleExpand(expandedKeys) {
      this.expandedKeys = expandedKeys;
      this.autoExpandParent = false;
    },
    handleScroll: function handleScroll(e) {
      // 
    },
    handleDragStart: function handleDragStart(e, _ref) {
      var key = _ref.key,
        title = _ref.title,
        eleType = _ref.eleType,
        pnId = _ref.pnId;
      // 取消冒泡
      e.stopPropagation();
      var props = {
        eleType: eleType,
        eleName: e.target.getAttribute('elename') || 'defaultName',
        className: 'Node',
        residualInfo: {
          area: title,
          id: key,
          pnId: pnId
        }
      };
      e.dataTransfer.effectAllowed = 'copy';
      e.dataTransfer.setData('ClassName', props.className);
      e.dataTransfer.setData('EleType', props.eleType);
      e.dataTransfer.setData('EleName', props.eleName);
      e.dataTransfer.setData('props', JSON.stringify(props));
      e.dataTransfer.setData('topology', true);
    },
    handleDragEnd: function handleDragEnd(e) {
      // 取消冒泡
      e.stopPropagation();
      if (e.dataTransfer.dropEffect == 'copy') this.dragKeys.push(e.target.id);
    }
  },
  computed: {
    nodeKeys: function nodeKeys() {
      return getNodeKeys$1([], this.treeData);
    }
  },
  watch: {
    searchValue: {
      immediate: true,
      handler: function handler(newValue, oldValue) {
        if (newValue != oldValue) {
          this.index = 0;
        }
      }
    }
  },
  render: function render() {
    var h = arguments[0];
    var that = this;
    var rootKeys = this.treeData.map(function (m) {
      return m.key;
    });
    function DeepDom(list, depth) {
      var arr = [];
      if (list.length > 0) {
        arr = list.map(function (t, index) {
          return h("tree-node", {
            "attrs": {
              "id": "".concat(t.key),
              "layer": depth + (index + 1),
              "disabled": that.dragKeys.includes(t.key),
              "draggable": !rootKeys.includes(t.key),
              "eleName": t.title
            },
            "nativeOn": {
              "dragstart": function dragstart(e) {
                return that.handleDragStart(e, {
                  key: t.key,
                  title: t.title,
                  eleType: t.eleType,
                  pnId: t.pnId
                });
              },
              "dragend": that.handleDragEnd
            },
            "key": t.key
          }, [h("template", {
            "slot": "title"
          }, [!rootKeys.includes(t.key) ? h("tooltip", {
            "attrs": {
              "title": "长按左键将其拖拽到画布"
            }
          }, [t.title.indexOf(that.searchValue) > -1 ? useTitle(t.title, that.searchValue) : h("span", [t.title])]) : t.title.indexOf(that.searchValue) > -1 ? useTitle(t.title, that.searchValue) : h("span", [t.title])]), t.children && DeepDom(t.children, depth++)]);
        });
      }
      return arr;
    }
    function useTitle(title, searchValue) {
      return h("span", [title.substr(0, title.indexOf(searchValue)), h("span", {
        "style": "color: #2e6ee7"
      }, [searchValue]), title.substr(title.indexOf(searchValue) + searchValue.length)]);
    }
    var treeData = this.treeData;
    return h("div", {
      "class": "resource"
    }, [h("input-search", {
      "class": "resource-search",
      "attrs": {
        "placeholder": "关键字搜索",
        "autocomplete": "off"
      },
      "on": {
        "pressEnter": that.handlePressEnter,
        "change": that.handleChange
      }
    }), treeData.length > 0 ? h("tree", {
      "attrs": {
        "defaultExpandAll": true,
        "id": "resource-tree",
        "expanded-keys": that.expandedKeys,
        "auto-expand-parent": that.autoExpandParent
      },
      "nativeOn": {
        "scroll": that.handleScroll
      },
      "class": "resource-tree",
      "on": {
        "expand": that.handleExpand
      }
    }, [DeepDom(treeData, 1)]) : h("div", {
      "class": "resource-blank"
    }, [h("span", ["\u6682\u65E0\u6570\u636E"])])]);
  }
};

/* script */
const __vue_script__$4 = script$4;

/* template */

  /* style */
  const __vue_inject_styles__$4 = function (inject) {
    if (!inject) return
    inject("data-v-ba12aa8c_0", { source: ".resource[data-v-ba12aa8c] {\n  width: 100%;\n  height: 100%;\n}\n.resource .resource-search[data-v-ba12aa8c] {\n  padding: 0 10px;\n}\n.resource .resource-tree[data-v-ba12aa8c] {\n  height: calc(50vh - 160px);\n  overflow-y: auto;\n  overflow-x: auto;\n}\n.resource .resource-blank[data-v-ba12aa8c] {\n  padding: 16px 0;\n  text-align: center;\n  color: #ccc;\n}\n", map: {"version":3,"sources":["Resource.vue"],"names":[],"mappings":"AAAA;EACE,WAAW;EACX,YAAY;AACd;AACA;EACE,eAAe;AACjB;AACA;EACE,0BAA0B;EAC1B,gBAAgB;EAChB,gBAAgB;AAClB;AACA;EACE,eAAe;EACf,kBAAkB;EAClB,WAAW;AACb","file":"Resource.vue","sourcesContent":[".resource {\n  width: 100%;\n  height: 100%;\n}\n.resource .resource-search {\n  padding: 0 10px;\n}\n.resource .resource-tree {\n  height: calc(50vh - 160px);\n  overflow-y: auto;\n  overflow-x: auto;\n}\n.resource .resource-blank {\n  padding: 16px 0;\n  text-align: center;\n  color: #ccc;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$4 = "data-v-ba12aa8c";
  /* module identifier */
  const __vue_module_identifier__$4 = undefined;
  /* functional template */
  const __vue_is_functional_template__$4 = undefined;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$4 = /*#__PURE__*/normalizeComponent(
    {},
    __vue_inject_styles__$4,
    __vue_script__$4,
    __vue_scope_id__$4,
    __vue_is_functional_template__$4,
    __vue_module_identifier__$4,
    false,
    createInjector,
    undefined,
    undefined
  );

__vue_component__$4.install = function (Vue) {
  Vue.component(__vue_component__$4.name, __vue_component__$4);
};

// 获取父节点的key值
var getParentKey = function getParentKey(key, tree) {
  var parentKey;
  for (var i = 0; i < tree.length; i++) {
    var node = tree[i];
    if (node.children) {
      if (node.children.some(function (item) {
        return item.key === key;
      })) {
        parentKey = node.key;
      } else if (getParentKey(key, node.children)) {
        parentKey = getParentKey(key, node.children);
      }
    }
  }
  return parentKey;
};

// 获取叶子节点的key值
var getNodeKeys = function getNodeKeys(nodeKeys, tree) {
  for (var i = 0; i < tree.length; i++) {
    var node = tree[i];
    if (node.children) {
      getNodeKeys(nodeKeys, node.children);
    } else {
      nodeKeys.push(node.key);
    }
  }
  return nodeKeys;
};
var script$3 = {
  name: 'Layer',
  props: {
    draggable: {
      type: Boolean,
      default: false
    },
    treeData: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  components: {
    InputSearch: antDesignVue.Input.Search,
    Tree: antDesignVue.Tree
  },
  data: function data() {
    return {
      index: 0,
      expandedKeys: [],
      matchedKeys: [],
      searchValue: '',
      autoExpandParent: true
    };
  },
  methods: {
    handleChange: function handleChange(e) {
      var _this = this;
      this.searchValue = e.target.value;
      this.autoExpandParent = true;
      // 拍平处理
      var dataList = [];
      var generateList = function generateList(data) {
        for (var i = 0; i < data.length; i++) {
          var node = data[i];
          var key = node.key,
            title = node.title;
          dataList.push({
            key: key,
            title: title
          });
          if (node.children) {
            generateList(node.children);
          }
        }
      };
      generateList(this.treeData);
      this.expandedKeys = dataList.map(function (item) {
        if (item.title.indexOf(e.target.value) > -1) {
          return getParentKey(item.key, _this.treeData);
        }
        return null;
      }).filter(function (item, i, self) {
        return item && self.indexOf(item) === i;
      });
      if (this.searchValue) {
        this.matchedKeys = dataList.filter(function (f) {
          return f.title.indexOf(_this.searchValue) > -1;
        }).map(function (m) {
          return m.key;
        });
        this.handleScrollIntoView(0);
      } else {
        if (document.getElementById("layer-tree")) {
          document.getElementById("layer-tree").scrollTop = 0;
        }
      }
    },
    handleScrollIntoView: function handleScrollIntoView(i) {
      var _this2 = this;
      if (this.matchedKeys.length > 0) {
        this.$nextTick(function () {
          var target = document.getElementById("".concat(_this2.matchedKeys[i % _this2.matchedKeys.length]));
          if (target) target.scrollIntoView();
        });
      }
    },
    handlePressEnter: function handlePressEnter(e) {
      if (e.target.value) {
        this.handleScrollIntoView(++this.index);
      }
    },
    handleExpand: function handleExpand(expandedKeys) {
      this.expandedKeys = expandedKeys;
      this.autoExpandParent = false;
    },
    handleDrop: function handleDrop(info) {
      // ;
    },
    handleDragEnter: function handleDragEnter(info) {
      // ;
    },
    handleClick: function handleClick(key) {
      this.$emit('choose', key);
    }
  },
  computed: {
    nodeKeys: function nodeKeys() {
      return getNodeKeys([], this.treeData);
    }
  },
  watch: {
    searchValue: {
      immediate: true,
      handler: function handler(newValue, oldValue) {
        if (newValue != oldValue) {
          this.index = 0;
        }
      }
    }
  }
};

/* script */
const __vue_script__$3 = script$3;

/* template */
var __vue_render__$3 = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "layer" },
    [
      _c("InputSearch", {
        staticClass: "layer-search",
        attrs: { placeholder: "关键字搜索", autocomplete: "off" },
        on: { change: _vm.handleChange, pressEnter: _vm.handlePressEnter },
      }),
      _vm._v(" "),
      _vm.treeData.length > 0
        ? _c("Tree", {
            staticClass: "layer-tree",
            attrs: {
              id: "layer-tree",
              draggable: _vm.draggable,
              treeData: _vm.treeData,
              "expanded-keys": _vm.expandedKeys,
              "auto-expand-parent": _vm.autoExpandParent,
            },
            on: {
              expand: _vm.handleExpand,
              dragenter: _vm.handleDragEnter,
              drop: _vm.handleDrop,
            },
            scopedSlots: _vm._u(
              [
                {
                  key: "title",
                  fn: function (ref) {
                    var title = ref.title;
                    var key = ref.key;
                    return [
                      _c("div", { attrs: { id: "" + key } }, [
                        title.indexOf(_vm.searchValue) > -1
                          ? _c(
                              "span",
                              {
                                on: {
                                  click: function ($event) {
                                    return _vm.handleClick(key)
                                  },
                                },
                              },
                              [
                                _vm._v(
                                  "\n          " +
                                    _vm._s(
                                      title.substr(
                                        0,
                                        title.indexOf(_vm.searchValue)
                                      )
                                    ) +
                                    "\n          "
                                ),
                                _c(
                                  "span",
                                  { staticStyle: { color: "#2e6ee7" } },
                                  [_vm._v(_vm._s(_vm.searchValue))]
                                ),
                                _vm._v(
                                  "\n          " +
                                    _vm._s(
                                      title.substr(
                                        title.indexOf(_vm.searchValue) +
                                          _vm.searchValue.length
                                      )
                                    ) +
                                    "\n        "
                                ),
                              ]
                            )
                          : _c(
                              "span",
                              {
                                on: {
                                  click: function ($event) {
                                    return _vm.handleClick(key)
                                  },
                                },
                              },
                              [_vm._v(_vm._s(title))]
                            ),
                      ]),
                    ]
                  },
                },
              ],
              null,
              false,
              906141426
            ),
          })
        : _c("div", { staticClass: "layer-blank" }, [
            _c("span", [_vm._v("暂无数据")]),
          ]),
    ],
    1
  )
};
var __vue_staticRenderFns__$3 = [];
__vue_render__$3._withStripped = true;

  /* style */
  const __vue_inject_styles__$3 = function (inject) {
    if (!inject) return
    inject("data-v-b9eca2ae_0", { source: ".layer[data-v-b9eca2ae] {\n  width: 100%;\n  height: 100%;\n}\n.layer .layer-search[data-v-b9eca2ae] {\n  padding: 0 10px;\n}\n.layer .layer-tree[data-v-b9eca2ae] {\n  height: calc(50vh - 160px);\n  overflow-y: auto;\n  overflow-x: auto;\n}\n.layer .layer-blank[data-v-b9eca2ae] {\n  padding: 16px 0;\n  text-align: center;\n  color: #ccc;\n}\n", map: {"version":3,"sources":["Layer.vue"],"names":[],"mappings":"AAAA;EACE,WAAW;EACX,YAAY;AACd;AACA;EACE,eAAe;AACjB;AACA;EACE,0BAA0B;EAC1B,gBAAgB;EAChB,gBAAgB;AAClB;AACA;EACE,eAAe;EACf,kBAAkB;EAClB,WAAW;AACb","file":"Layer.vue","sourcesContent":[".layer {\n  width: 100%;\n  height: 100%;\n}\n.layer .layer-search {\n  padding: 0 10px;\n}\n.layer .layer-tree {\n  height: calc(50vh - 160px);\n  overflow-y: auto;\n  overflow-x: auto;\n}\n.layer .layer-blank {\n  padding: 16px 0;\n  text-align: center;\n  color: #ccc;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$3 = "data-v-b9eca2ae";
  /* module identifier */
  const __vue_module_identifier__$3 = undefined;
  /* functional template */
  const __vue_is_functional_template__$3 = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$3 = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$3, staticRenderFns: __vue_staticRenderFns__$3 },
    __vue_inject_styles__$3,
    __vue_script__$3,
    __vue_scope_id__$3,
    __vue_is_functional_template__$3,
    __vue_module_identifier__$3,
    false,
    createInjector,
    undefined,
    undefined
  );

__vue_component__$3.install = function (Vue) {
  Vue.component(__vue_component__$3.name, __vue_component__$3);
};

//
var script$2 = {
  name: 'ToolBox',
  props: {
    icon: {
      type: String,
      default: ''
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },
  components: {
    Avatar: antDesignVue.Avatar
  },
  methods: {
    handleClick: function handleClick() {
      if (this.disabled) {
        return false;
      } else {
        this.$emit('click');
      }
    }
  },
  computed: {
    computedClass: function computedClass() {
      return this.disabled ? 'tool-box tool-box-disabled' : 'tool-box';
    }
  }
};

/* script */
const __vue_script__$2 = script$2;

/* template */
var __vue_render__$2 = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { class: _vm.computedClass, on: { click: _vm.handleClick } },
    [
      _c("Avatar", { staticClass: "tool-box-icon", attrs: { src: _vm.icon } }),
      _vm._v(" "),
      _c("div", { staticClass: "tool-box-title" }, [_vm._t("title")], 2),
    ],
    1
  )
};
var __vue_staticRenderFns__$2 = [];
__vue_render__$2._withStripped = true;

  /* style */
  const __vue_inject_styles__$2 = function (inject) {
    if (!inject) return
    inject("data-v-26d0d266_0", { source: ".tool-box[data-v-26d0d266] {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  padding: 0 16px;\n  color: #666;\n}\n.tool-box .tool-box-icon[data-v-26d0d266] {\n  width: 32px;\n  height: 32px;\n}\n.tool-box .tool-box-title[data-v-26d0d266] {\n  margin-top: 2px;\n  font-size: 12px;\n  min-width: 0;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.tool-box-disabled[data-v-26d0d266] {\n  pointer-events: none;\n  color: #999;\n  cursor: not-allowed;\n}\n.tool-box[data-v-26d0d266]:hover {\n  cursor: pointer;\n}\n", map: {"version":3,"sources":["ToolBox.vue"],"names":[],"mappings":"AAAA;EACE,aAAa;EACb,sBAAsB;EACtB,mBAAmB;EACnB,uBAAuB;EACvB,eAAe;EACf,WAAW;AACb;AACA;EACE,WAAW;EACX,YAAY;AACd;AACA;EACE,eAAe;EACf,eAAe;EACf,YAAY;EACZ,mBAAmB;EACnB,gBAAgB;EAChB,uBAAuB;AACzB;AACA;EACE,oBAAoB;EACpB,WAAW;EACX,mBAAmB;AACrB;AACA;EACE,eAAe;AACjB","file":"ToolBox.vue","sourcesContent":[".tool-box {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  padding: 0 16px;\n  color: #666;\n}\n.tool-box .tool-box-icon {\n  width: 32px;\n  height: 32px;\n}\n.tool-box .tool-box-title {\n  margin-top: 2px;\n  font-size: 12px;\n  min-width: 0;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.tool-box-disabled {\n  pointer-events: none;\n  color: #999;\n  cursor: not-allowed;\n}\n.tool-box:hover {\n  cursor: pointer;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$2 = "data-v-26d0d266";
  /* module identifier */
  const __vue_module_identifier__$2 = undefined;
  /* functional template */
  const __vue_is_functional_template__$2 = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$2 = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$2, staticRenderFns: __vue_staticRenderFns__$2 },
    __vue_inject_styles__$2,
    __vue_script__$2,
    __vue_scope_id__$2,
    __vue_is_functional_template__$2,
    __vue_module_identifier__$2,
    false,
    createInjector,
    undefined,
    undefined
  );

//
//
//
//
//
//

var script$1 = {
  name: 'ToolBoxGroup',
  props: {
    left: {
      type: Boolean,
      default: true
    },
    right: {
      type: Boolean,
      default: true
    }
  },
  computed: {
    toolBoxGroupClass: function toolBoxGroupClass() {
      if (!this.left && !this.right) {
        return "tool-box-group tool-box-group-noneleft tool-box-group-noneright";
      } else if (!this.left && this.right) {
        return "tool-box-group tool-box-group-noneleft";
      } else if (!this.right && this.left) {
        return "tool-box-group tool-box-group-noneright";
      } else {
        return "tool-box-group";
      }
    }
  }
};

/* script */
const __vue_script__$1 = script$1;

/* template */
var __vue_render__$1 = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("div", { class: _vm.toolBoxGroupClass }, [_vm._t("default")], 2)
};
var __vue_staticRenderFns__$1 = [];
__vue_render__$1._withStripped = true;

  /* style */
  const __vue_inject_styles__$1 = function (inject) {
    if (!inject) return
    inject("data-v-dd3dd2f0_0", { source: ".tool-box-group[data-v-dd3dd2f0] {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  height: 100%;\n}\n.tool-box-group .tool-box[data-v-dd3dd2f0] {\n  padding: 0 8px;\n}\n.tool-box-group > .tool-box[data-v-dd3dd2f0]:first-child {\n  border-left: 1px dashed rgba(151, 151, 151, 0.29);\n  padding-left: 16px;\n}\n.tool-box-group.tool-box-group-noneleft > .tool-box[data-v-dd3dd2f0]:first-child {\n  border-left: none;\n}\n.tool-box-group > .tool-box[data-v-dd3dd2f0]:last-child {\n  border-right: 1px dashed rgba(151, 151, 151, 0.29);\n  padding-right: 16px;\n}\n.tool-box-group.tool-box-group-noneright > .tool-box[data-v-dd3dd2f0]:last-child {\n  border-right: none;\n}\n", map: {"version":3,"sources":["ToolBoxGroup.vue"],"names":[],"mappings":"AAAA;EACE,aAAa;EACb,mBAAmB;EACnB,mBAAmB;EACnB,YAAY;AACd;AACA;EACE,cAAc;AAChB;AACA;EACE,iDAAiD;EACjD,kBAAkB;AACpB;AACA;EACE,iBAAiB;AACnB;AACA;EACE,kDAAkD;EAClD,mBAAmB;AACrB;AACA;EACE,kBAAkB;AACpB","file":"ToolBoxGroup.vue","sourcesContent":[".tool-box-group {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  height: 100%;\n}\n.tool-box-group .tool-box {\n  padding: 0 8px;\n}\n.tool-box-group > .tool-box:first-child {\n  border-left: 1px dashed rgba(151, 151, 151, 0.29);\n  padding-left: 16px;\n}\n.tool-box-group.tool-box-group-noneleft > .tool-box:first-child {\n  border-left: none;\n}\n.tool-box-group > .tool-box:last-child {\n  border-right: 1px dashed rgba(151, 151, 151, 0.29);\n  padding-right: 16px;\n}\n.tool-box-group.tool-box-group-noneright > .tool-box:last-child {\n  border-right: none;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$1 = "data-v-dd3dd2f0";
  /* module identifier */
  const __vue_module_identifier__$1 = undefined;
  /* functional template */
  const __vue_is_functional_template__$1 = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__$1 = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__$1, staticRenderFns: __vue_staticRenderFns__$1 },
    __vue_inject_styles__$1,
    __vue_script__$1,
    __vue_scope_id__$1,
    __vue_is_functional_template__$1,
    __vue_module_identifier__$1,
    false,
    createInjector,
    undefined,
    undefined
  );

//
//
//
//
//
//

var script = {
  name: 'Tool',
  props: {}
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("div", { staticClass: "tool" }, [_vm._t("default")], 2)
};
var __vue_staticRenderFns__ = [];
__vue_render__._withStripped = true;

  /* style */
  const __vue_inject_styles__ = function (inject) {
    if (!inject) return
    inject("data-v-4a67872c_0", { source: ".tool[data-v-4a67872c] {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  height: 100%;\n  width: 100%;\n  overflow: hidden;\n}\n", map: {"version":3,"sources":["Tool.vue"],"names":[],"mappings":"AAAA;EACE,aAAa;EACb,mBAAmB;EACnB,mBAAmB;EACnB,YAAY;EACZ,WAAW;EACX,gBAAgB;AAClB","file":"Tool.vue","sourcesContent":[".tool {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  height: 100%;\n  width: 100%;\n  overflow: hidden;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__ = "data-v-4a67872c";
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = /*#__PURE__*/normalizeComponent(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    createInjector,
    undefined,
    undefined
  );

__vue_component__$2.install = function (Vue) {
  Vue.component(__vue_component__$2.name, __vue_component__$2);
};
__vue_component__$1.install = function (Vue) {
  Vue.component(__vue_component__$1.name, __vue_component__$1);
};
__vue_component__.install = function (Vue) {
  Vue.component(__vue_component__.name, __vue_component__);
};

exports.Attr = __vue_component__$j;
exports.Canvas = __vue_component__$c;
exports.Data = __vue_component__$b;
exports.GroupAttr = __vue_component__$d;
exports.Layer = __vue_component__$3;
exports.Library = __vue_component__$5;
exports.LibraryBox = __vue_component__$7;
exports.LibraryBoxGroup = __vue_component__$6;
exports.LinkAttr = __vue_component__$e;
exports.LinkData = __vue_component__$9;
exports.NodeAttr = __vue_component__$h;
exports.NodeData = __vue_component__$8;
exports.Resource = __vue_component__$4;
exports.SubNetworkAttr = __vue_component__$g;
exports.Tool = __vue_component__;
exports.ToolBox = __vue_component__$2;
exports.ToolBoxGroup = __vue_component__$1;
