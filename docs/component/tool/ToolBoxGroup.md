# ToolBoxGroup

## 基础用法

这是一个工具区区块的组件案例

> demo 通过 `icon` 属性设置工具区块的图标

```html
<template>
  <tool>
    <tool-box-group>
        <tool-box v-for="item in groups" :key="item.icon" :icon="item.icon">
            <span>{{item.title}}</span>
        </tool-box>
    </tool-box-group>
  </tool>
</template>

<script>
  export default {
    data() {
      return {
        groups: [
            {
                icon: 'http://10.186.2.55:8148/tgcos/582717604620:vwaver/icon-group.svg',
                title: '创建组'
            },
            {
                icon: 'http://10.186.2.55:8148/tgcos/582717604620:vwaver/icon-subnetwork.svg',
                title: '创建子网'
            },
            {
                icon: 'http://10.186.2.55:8148/tgcos/582717604620:vwaver/icon-link.svg',
                title: '连接线'
            },
            {
                icon: 'http://10.186.2.55:8148/tgcos/582717604620:vwaver/icon-alignment.svg',
                title: '对齐'
            }
        ]
      }
    },
  };
</script>
```

:::

## 属性参数

|参数|说明|类型|可选值|默认值|
|:-:|:-:|:-:|:-:|:-:|
|left|左侧边框|``Boolean``|`true` 或 `false` |`true`|
|right|右侧边框|``Boolean``|`true` 或 `false`|`true`|