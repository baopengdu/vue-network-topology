import Canvas from './Canvas.vue';

Canvas.install = function(Vue) {
    Vue.component(Canvas.name, Canvas)
}

export default Canvas;