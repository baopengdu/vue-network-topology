# LibraryBox

## 基础用法

这是一个图形库区块的组件案例

> 通过 `icon` 属性设置图形库区块的图标，通过 `title` 设置图形库区块的名称

```html
<template>
  <library-box :icon="icon" :title="title" />
</template>

<script>
  export default {
    data() {
      return {
        icon: 'http://10.186.2.55:8148/tgcos/364583956093:cdn/topology/style1/element-style1-custom_room.png',
        title: '客户机房'
      }
    },
  };
</script>
```

:::

## 属性参数

|参数|说明|类型|可选值|默认值|
|:-:|:-:|:-:|:-:|:-:|
|icon|图标连接|``String``|--|--|
|title|图形库区块名称|``String``|--|--|