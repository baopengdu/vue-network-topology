<p align="center"><a href="https://gitee.com/VLeeDesignTheory/vue-network-topology.git" target="_blank" rel="noopener noreferrer"><img width="200" src="assets/logo.png" alt="logo"></a></p>

# vue-network-topology

## 介绍

> 一款基于Vue2的网络拓扑组件

## 软件架构

![总体架构](./assets/architecture.png)

### 属性模块

![属性模块](./assets/attr.png)
### 数据模块

![数据模块](./assets/data.png)
### 拓扑树模块

![拓扑树模块](./assets/layer.png)
### 资源树模块

![资源模块](./assets/resource.png)

### 图形库模块

![图形库模块](./assets/library.png)
### 工具模块

![工具模块](./assets/tool.png)

## 设计指南

## 概览

[General](./docs/guide/General.md)
## 原则

[Principle](./docs/guide/Principle.md)
## 安装教程

1.  下载

```
npm install vue-network-topology
```

2.  引入

```js
import * as VueNetworkTopology from 'vue-network-topology'

Object.keys(VueNetworkTopology).forEach(key => {
  Vue.use(VueNetworkTopology[`${key}`])
})
```

3.  使用

```vue
<template>
    <Resource :treeData="treeData"/>
</template>

<script>
export default {
    data() {
        return {
            treeData: []
        }
    }
}
</script>

<style></style>
```

## 组件说明

![组件](./assets/component.png)

|组件|名称|备注|
|:-:|:-:|:-:|
|属性组件|[Attr](./docs/component/attr/Attr.md)|`import { Attr } from 'vue-network-topology'`|
|属性节点组件|[NodeAttr](./docs/component/attr/NodeAttr.md)|`import { NodeAttr } from 'vue-network-topology'`|
|属性线组件|[LinkAttr](./docs/component/attr/LinkAttr.md)|`import { LinkAttr } from 'vue-network-topology'`|
|属性组组件|[GroupAttr](./docs/component/attr/GroupAttr.md)|`import { GroupAttr } from 'vue-network-topology'`|
|画布组件|[Canvas](./docs/component/canvas/Canvas.md)|`import { Canvas } from 'vue-network-topology'`|
|数据组件|[Data](./docs/component/data/Data.md)|`import { Data } from 'vue-network-topology'`|
|数据节点组件|[NodeData](./docs/component/data/NodeData.md)|`import { NodeData } from 'vue-network-topology'`|
|数据线组件|[LinkData](./docs/component/data/LinkData.md)|`import { LinkData} from 'vue-network-topology'`|
|拓扑树组件|[Layer](./docs/component/layer/Layer.md)|`import { Layer } from 'vue-network-topology'`|
|图形库组件|[Library](./docs/component/library/Library.md)|`import { Library } from 'vue-network-topology'`|
|图形库区块组件|[LibraryBox](./docs/component/library/LibraryBox.md)|`import { LibraryBox } from 'vue-network-topology'`|
|图形库区块组组件|[LibraryBoxGroup](./docs/component/library/LibraryBoxGroup.md)|`import { LibraryBoxGroup } from 'vue-network-topology'`|
|资源树组件|[Resource](./docs/component/resource/Resource.md)|`import { Resource } from 'vue-network-topology'`|
|工具组件|[Tool](./docs/component/tool/Tool.md)|`import { Tool } from 'vue-network-topology'`|
|工具区块组件|[ToolBox](./docs/component/tool/ToolBox.md)|`import { ToolBox } from 'vue-network-topology'`|
|工具区块组组件|[ToolBoxGroup](./docs/component/tool/ToolBoxGroup.md)|`import { ToolBoxGroup } from 'vue-network-topology'`|
## 依赖说明

|依赖|依赖包|备注|
|:-:|:-:|:-:|
|vue|[vue 2.x](https://www.npmjs.com/package/vue)|基于vue2的运行时进行构建|
|ant-design-vue|[ant-design-vue 1.x](https://www.npmjs.com/package/ant-design-vue)|基于ant-design-vue的组件库进行构建，主题色修改等请参考ant-desgin-vue的官方文档|

## 最佳实践

### 大客户网络管家拓扑编辑器

[Editor1](./docs/example/editor/Editor1.md)

### 专网管家拓扑编辑器

[Editor1](./docs/example/editor/Editor2.md)
### 网络运营平台拓扑可视

[Visual](./docs/example/visual/Visual.md)
## 参考文献

- [Vue 2.x官方文档](https://v2.cn.vuejs.org/)
- [Ant-Design-Vue 1.x官方文档](https://1x.antdv.com/docs/vue/introduce-cn/)

## 许可说明

[MIT](http://opensource.org/licenses/MIT)

Copyright (c) 2023-present Victor Lee
