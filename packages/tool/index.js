import ToolBox from './components/ToolBox.vue';
import ToolBoxGroup from './components/ToolBoxGroup.vue';
import Tool from './Tool.vue';

ToolBox.install = function(Vue) {
    Vue.component(ToolBox.name, ToolBox)
}

ToolBoxGroup.install = function(Vue) {
    Vue.component(ToolBoxGroup.name, ToolBoxGroup)
}

Tool.install = function(Vue) {
    Vue.component(Tool.name, Tool)
}

export {
    ToolBox,
    ToolBoxGroup,
    Tool,
};