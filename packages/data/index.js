import Data from './Data.vue';
import LinkData from './components/LinkData.vue';
import NodeData from './components/NodeData.vue';

LinkData.install = function(Vue) {
    Vue.component(LinkData.name, LinkData)
}

NodeData.install = function(Vue) {
    Vue.component(NodeData.name, NodeData)
}

Data.install = function(Vue) {
    Vue.component(Data.name, Data)
}

export {
    LinkData,
    NodeData,
    Data
};