import Attr from './Attr.vue';
import NodeAttr from './components/NodeAttr.vue';
import SubNetworkAttr from './components/SubNetworkAttr.vue';
import LinkAttr from './components/LinkAttr.vue';
import GroupAttr from './components/GroupAttr.vue';

SubNetworkAttr.install = function(Vue) {
    Vue.component(SubNetworkAttr.name, SubNetworkAttr)
}

NodeAttr.install = function(Vue) {
    Vue.component(NodeAttr.name, NodeAttr)
}

GroupAttr.install = function(Vue) {
    Vue.component(GroupAttr.name, GroupAttr)
}

LinkAttr.install = function(Vue) {
    Vue.component(LinkAttr.name, LinkAttr)
}

Attr.install = function(Vue) {
    Vue.component(Attr.name, Attr)
}

export {
    SubNetworkAttr,
    NodeAttr,
    GroupAttr,
    LinkAttr,
    Attr
};