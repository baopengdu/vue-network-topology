# LibraryBoxGroup

## 基础用法

这是一个图形库区块组的组件案例

> 通过 `name` 属性设置图形库区块组的名称

```html
<template>
  <tool>
    <library-box-group :name="name">
      LibraryBoxGroup
    </library-box-group>
  </tool>
</template>

<script>
  export default {
    data() {
      return {
        name: "机房"
      }
    },
  };
</script>
```

:::

## 属性参数

|参数|说明|类型|可选值|默认值|
|:-:|:-:|:-:|:-:|:-:|
|name|图形区块组名称|``String``|--|--|