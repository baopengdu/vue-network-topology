import LibraryBox from './components/LibraryBox.vue';

import LibraryBoxGroup from './components/LibraryBoxGroup.vue';

import Library from './Library.vue';

Library.install = function(Vue) {
    Vue.component(Library.name, Library)
}

LibraryBox.install = function(Vue) {
    Vue.component(LibraryBox.name, LibraryBox)
}


LibraryBoxGroup.install = function(Vue) {
    Vue.component(LibraryBoxGroup.name, LibraryBoxGroup)
}


export {
    Library,
    LibraryBox,
    LibraryBoxGroup
};