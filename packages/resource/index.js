import Resource from './Resource.vue';

Resource.install = function(Vue) {
    Vue.component(Resource.name, Resource)
}

export default Resource;